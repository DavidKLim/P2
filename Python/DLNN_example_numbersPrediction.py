# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

# %%
import tensorflow as tf
mnist = tf.keras.datasets.mnist     # 28x28 images digits 0-9
(x_train,y_train),(x_test,y_test) = mnist.load_data()

x_train = tf.keras.utils.normalize(x_train,axis=1)
x_test = tf.keras.utils.normalize(x_test,axis=1)

model = tf.keras.models.Sequential()    # Specify Sequential (feed forward) model
model.add(tf.keras.layers.Flatten())    # first (input) layer: flatten
model.add(tf.keras.layers.Dense(128,activation=tf.nn.relu))  # hidden layer with 128 units and rectified linear activation function
model.add(tf.keras.layers.Dense(128,activation=tf.nn.relu))  # hidden layer with 128 units and rectified linear activation function
model.add(tf.keras.layers.Dense(10,activation=tf.nn.softmax))  # output layer with 10 units (classifications) and softmax activation function (output probabilities)
model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])
# ADAM optimizer (default) can substitute with stochastic gradient or others
# want to minimize loss (relationship between loss & accuracy): typical is sparse cat crossentropy. can do binary or etc
# what metric to track as we go

model.fit(x_train, y_train, epochs=3)   # epochs: number of times(?)
# %%

# %%
# NN great at fitting. question: did it overfit? (huge delta: overfitted. dial it back)
val_loss,val_acc = model.evaluate(x_test,y_test)
print(val_loss,val_acc)
# %%

# %% Save model
model.save('epic_num_reader.model')
# %%

# %% Load model
new_model = tf.keras.models.load_model('epic_num_reader.model')
# %%

# %% Important: Predictions always take a list!
predictions = new_model.predict([x_test])
# %%

#%% 
import numpy as np
print(np.argmax(predictions[1]))
#%%

# %% matplotlib package can plot image data. black-white image by plt.cm.binary
import matplotlib.pyplot as plt
plt.imshow(x_train[0],cmap=plt.cm.binary)
plt.imshow(x_test[3])
# %%
