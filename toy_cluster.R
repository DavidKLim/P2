
# need to re-run NIMIWAE to cluster w k-means (can use existing method, and just re-run the test with opt params)
rerun_NIMIWAE_opt_test = function(dataset, mechanism=c("MCAR","MAR","MNAR"), miss_pct=25, phi0=5, sim_index=1){
  library(reticulate)
  source_python("toy_networks.py")
  source("toy_sim.R")
  data_name=sprintf("Results/%s/phi%d/sim%d/data_%s_%d.RData",dataset,phi0,sim_index,mechanism,miss_pct)
  load(data_name)
  datas = split(data.frame(data), g)        # split by $train, $test, and $valid
  Missings = split(data.frame(Missing), g)
  probs_Missing = split(data.frame(prob_Missing),g)
  norm_means=colMeans(datas$train); norm_sds=apply(datas$train,2,sd)
  
  res_name=sprintf("Results/%s/phi%d/sim%d/res_NIMIWAE_%s_%d_IWAE_rzF.RData",dataset,phi0,sim_index,mechanism,miss_pct)
  load(res_name)
  
  
  # optimal model and parameters are all saved in the res_test of NIMIWAE
  # opt_train = res_NIMIWAE  #doesn't save from previous session
  opt_params = res_NIMIWAE$opt_params
  
  time_elapsed = res_NIMIWAE$time
  
  rdeponz=F; input_r="r"; covars_r=rep(1L,ncol(datas$test)); learn_r=T; sparse="none"; dropout_pct=NULL; include_xo=T
  partial_opt=F; arch="IWAE"; nits=1L; nGibbs=0L; add_miss_term=F; draw_xobs=F; draw_xmiss=T; pre_impute_value=0L
  phi0=fit_missing$params[[1]]$phi0; phi=fit_missing$params[[1]]$phi; dec_distrib="Normal"
  
  np = import("numpy")
  opt_train = run_NIMIWAE_toy_N16(rdeponz=rdeponz, input_r=input_r, data=np$array(datas$train),Missing=np$array(Missings$train),probMissing=np$array(probs_Missing$train),
                  covars_r=np$array(covars_r), norm_means=np$array(norm_means), norm_sds=np$array(norm_sds), learn_r=learn_r,
                  L1_weight=0,L2_weight=0, n_hidden_layers=if(is.null(opt_params$'n_hidden_layers')){1L}else{opt_params$'n_hidden_layers'}, n_hidden_layers_r=0L,
                  unnorm=F,sparse=sparse, dropout_pct=dropout_pct,prune_pct=NULL,covars_miss=NULL,impute_bs=opt_params$'bs',include_xo=include_xo,
                  partial_opt=partial_opt, arch=arch, nits=nits, nGibbs=nGibbs,
                  add_miss_term=add_miss_term,draw_xobs=draw_xobs,draw_xmiss=draw_xmiss,
                  pre_impute_value=pre_impute_value,h1=opt_params$'h1',h2=opt_params$'h2',h3=opt_params$'h3',h4=opt_params$'h4',beta=1,beta_anneal_rate=0,
                  phi0=phi0, phi=phi, saved_model=NULL, dec_distrib=dec_distrib, train=1L,
                  sigma='elu',bs = opt_params$'bs', n_epochs = opt_params$'n_epochs',lr=opt_params$'lr',niw=opt_params$'niw',dim_z=opt_params$'dim_z',L=opt_params$'L', M=opt_params$'M')
  
  res_NIMIWAE = run_NIMIWAE_toy_N16(rdeponz=rdeponz, input_r=input_r, data=np$array(datas$test),Missing=np$array(Missings$test),probMissing=np$array(probs_Missing$test),
                 covars_r=np$array(covars_r), norm_means=np$array(norm_means), norm_sds=np$array(norm_sds), learn_r=learn_r,
                 L1_weight=0,L2_weight=0, n_hidden_layers=if(is.null(opt_params$'n_hidden_layers')){1L}else{opt_params$'n_hidden_layers'}, n_hidden_layers_r=0L,
                 unnorm=F,sparse=sparse, dropout_pct=dropout_pct,prune_pct=NULL,covars_miss=NULL,impute_bs=opt_params$'bs',include_xo=include_xo,
                 partial_opt=partial_opt, arch=arch, nits=nits, nGibbs=nGibbs,
                 add_miss_term=add_miss_term,draw_xobs=draw_xobs,draw_xmiss=draw_xmiss,
                 pre_impute_value=pre_impute_value,h1=opt_params$'h1',h2=opt_params$'h2',h3=opt_params$'h3',h4=opt_params$'h4',beta=1,beta_anneal_rate=0,
                 phi0=phi0, phi=phi, saved_model=opt_train$'saved_model', dec_distrib=dec_distrib, train=0L,
                 sigma='elu',bs = opt_params$'bs', n_epochs = 2L,lr=opt_params$'lr',niw=opt_params$'niw',dim_z=opt_params$'dim_z',L=opt_params$'L', M=opt_params$'M')
  
  res_NIMIWAE$opt_params=opt_params
  
  res_NIMIWAE$xhat_rev = reverse_norm_MIWAE(res_NIMIWAE$xhat,norm_means,norm_sds) # reverse normalization for proper comparison
  
  
  nMB = length(res_NIMIWAE$xhat_fits)
  for(ii in 1:nMB){
    if(ii==1){
      zgivenx_flat=res_NIMIWAE$xhat_fits[[ii]]$xhat_fit$zgivenx_flat; imp_weights = res_NIMIWAE$xhat_fits[[ii]]$xhat_fit$imp_weights
      next
    }
    zgivenx_flat = rbind(zgivenx_flat, res_NIMIWAE$xhat_fits[[ii]]$xhat_fit$zgivenx_flat)
    imp_weights = cbind(imp_weights, res_NIMIWAE$xhat_fits[[ii]]$xhat_fit$imp_weights)
  }
  
  res_NIMIWAE$zgivenx_flat = zgivenx_flat; res_NIMIWAE$imp_weights=imp_weights
  res_NIMIWAE$xhat_fits=NULL
  save(res_NIMIWAE, file=res_name)
}

mechanisms=c("MAR","MNAR"); sim_index=1
for(m in 1:length(mechanisms)){ for(s in 1:length(sim_index)){ rerun_NIMIWAE_opt_test(dataset="TOYZ",mechanism=mechanisms[m],miss_pct=25,phi0=5,sim_index=sim_index[s]) }}
mechanisms=c("MCAR","MAR","MNAR"); sim_index=1:5
for(m in 1:length(mechanisms)){ for(s in 1:length(sim_index)){ rerun_NIMIWAE_opt_test(dataset="TOYZ2",mechanism=mechanisms[m],miss_pct=25,phi0=5,sim_index=sim_index[s]) }}
mechanisms=c("MCAR","MAR","MNAR"); sim_index=1
datasets=c("BANKNOTE", "BREAST", "CONCRETE", "RED", "WHITE","YEAST","HEPMASS","MINIBOONE")
for(d in 1:length(datasets)){ for(m in 1:length(mechanisms)){ for(s in 1:length(sim_index)){
  rerun_NIMIWAE_opt_test(dataset=datasets[d],mechanism=mechanisms[m],miss_pct=25,phi0=5,sim_index=sim_index[s]) }}}
datasets=c("Physionet_mean"); mechanisms=c(NA); sim_index=1
for(d in 1:length(datasets)){ for(m in 1:length(mechanisms)){ for(s in 1:length(sim_index)){
  rerun_NIMIWAE_opt_test(dataset=datasets[d],mechanism=mechanisms[m],miss_pct=25,phi0=5,sim_index=sim_index[s]) }}}






### doing the clustering on zgivenx_flats (recalculated above)
load("Results/BANKNOTE/phi5/sim1/data_MNAR_25.RData")
classes = fit_data$classes
datas = split(data.frame(data), g)        # split by $train, $test, and $valid
Missings = split(data.frame(Missing), g)
probs_Missing = split(data.frame(prob_Missing),g)
norm_means=colMeans(datas$train); norm_sds=apply(datas$train,2,sd)
true_classes = split(classes,g)$test
if(method=="NIMIWAE"){
  load("Results/BANKNOTE/phi5/sim1/res_NIMIWAE_MNAR_25_IWAE_rzF.RData")
  zgivenx_flat = res_NIMIWAE$zgivenx_flat
  
  b<-rep(1:(nrow(zgivenx_flat)/5),each=5)
  x=matrix(nrow=nrow(zgivenx_flat)/5,ncol=ncol(zgivenx_flat))
  for(d in 1:ncol(zgivenx_flat)){
    x[,d]<-ave(zgivenx_flat[,d], b, FUN=mean)[seq(1,nrow(zgivenx_flat),5)]  #average over the niws for each obs
  }
  
  cls = kmeans(x,2)$cluster
  
}else if(method=="MIWAE"){
  zgivenx_flat = res_MIWAE$zgivenx_flat
  cls = kmeans(zgivenx_flat)$cluster
}else if(method=="HIVAE"){
  cls = apply(res_HIVAE$s_aux,1,function(x)which(x==1))
}