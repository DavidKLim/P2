## Misc functions:
def get_pandas_path():
  import pandas
  pandas_path = pandas.__path__[0]
  return pandas_path
    
## GAIN (Yoon et al 2018):
def run_GAIN(Data,Missing,mb_size=128,p_hint=0.9,alpha=10,train_rate=0.8,n_epochs=500):
  import tensorflow as tf
  import numpy as np
  import os
  from tqdm import tqdm
  # Parameters
  No = len(Data)
  Dim = len(Data[0,:])
  
  # Hidden state dimensions
  H_Dim1 = Dim
  H_Dim2 = Dim
  
  # Normalization (0 to 1)
  Min_Val = np.zeros(Dim)
  Max_Val = np.zeros(Dim)
  
  for i in range(Dim):
      Min_Val[i] = np.min(Data[:,i])
      Data[:,i] = Data[:,i] - np.min(Data[:,i])
      Max_Val[i] = np.max(Data[:,i])
      Data[:,i] = Data[:,i] / (np.max(Data[:,i]) + 1e-6)    
  
      
  #%% Train Test Division    
     
  idx = np.random.permutation(No)
  
  Train_No = int(No * train_rate)
  Test_No = No - Train_No
      
  # Train / Test Features
  trainX = Data[idx[:Train_No],:]
  testX = Data[idx[Train_No:],:]
  
  # Train / Test Missing Indicators
  trainM = Missing[idx[:Train_No],:]
  testM = Missing[idx[Train_No:],:]
  
  #%% Necessary Functions
  
  # 1. Xavier Initialization Definition
  def xavier_init(size):
      in_dim = size[0]
      xavier_stddev = 1. / tf.sqrt(in_dim / 2.)
      return tf.random_normal(shape = size, stddev = xavier_stddev)
      
  # Hint Vector Generation
  def sample_M(m, n, p):
      A = np.random.uniform(0., 1., size = [m, n])
      B = A > p
      C = 1.*B
      return C
     
  '''
  GAIN Consists of 3 Components
  - Generator
  - Discriminator
  - Hint Mechanism
  '''   
     
  #%% GAIN Architecture   
     
  #%% 1. Input Placeholders
  # 1.1. Data Vector
  X = tf.placeholder(tf.float32, shape = [None, Dim])
  # 1.2. Mask Vector 
  M = tf.placeholder(tf.float32, shape = [None, Dim])
  # 1.3. Hint vector
  H = tf.placeholder(tf.float32, shape = [None, Dim])
  # 1.4. X with missing values
  New_X = tf.placeholder(tf.float32, shape = [None, Dim])
  
  #%% 2. Discriminator
  D_W1 = tf.Variable(xavier_init([Dim*2, H_Dim1]))     # Data + Hint as inputs
  D_b1 = tf.Variable(tf.zeros(shape = [H_Dim1]))
  
  D_W2 = tf.Variable(xavier_init([H_Dim1, H_Dim2]))
  D_b2 = tf.Variable(tf.zeros(shape = [H_Dim2]))
  
  D_W3 = tf.Variable(xavier_init([H_Dim2, Dim]))
  D_b3 = tf.Variable(tf.zeros(shape = [Dim]))       # Output is multi-variate
  
  theta_D = [D_W1, D_W2, D_W3, D_b1, D_b2, D_b3]
  
  #%% 3. Generator
  G_W1 = tf.Variable(xavier_init([Dim*2, H_Dim1]))     # Data + Mask as inputs (Random Noises are in Missing Components)
  G_b1 = tf.Variable(tf.zeros(shape = [H_Dim1]))
  
  G_W2 = tf.Variable(xavier_init([H_Dim1, H_Dim2]))
  G_b2 = tf.Variable(tf.zeros(shape = [H_Dim2]))
  
  G_W3 = tf.Variable(xavier_init([H_Dim2, Dim]))
  G_b3 = tf.Variable(tf.zeros(shape = [Dim]))
  
  theta_G = [G_W1, G_W2, G_W3, G_b1, G_b2, G_b3]
  
  #%% GAIN Function
  
  #%% 1. Generator
  def generator(new_x,m):
      inputs = tf.concat(axis = 1, values = [new_x,m])  # Mask + Data Concatenate
      G_h1 = tf.nn.relu(tf.matmul(inputs, G_W1) + G_b1)
      G_h2 = tf.nn.relu(tf.matmul(G_h1, G_W2) + G_b2)   
      G_prob = tf.nn.sigmoid(tf.matmul(G_h2, G_W3) + G_b3) # [0,1] normalized Output
      
      return G_prob
      
  #%% 2. Discriminator
  def discriminator(new_x, h):
      inputs = tf.concat(axis = 1, values = [new_x,h])  # Hint + Data Concatenate
      D_h1 = tf.nn.relu(tf.matmul(inputs, D_W1) + D_b1)  
      D_h2 = tf.nn.relu(tf.matmul(D_h1, D_W2) + D_b2)
      D_logit = tf.matmul(D_h2, D_W3) + D_b3
      D_prob = tf.nn.sigmoid(D_logit)  # [0,1] Probability Output
      
      return D_prob
  
  #%% 3. Other functions
  # Random sample generator for Z
  def sample_Z(m, n):
      return np.random.uniform(0., 0.01, size = [m, n])        
  
  # Mini-batch generation
  def sample_idx(m, n):
      A = np.random.permutation(m)
      idx = A[:n]
      return idx
  
  #%% Structure
  # Generator
  G_sample = generator(New_X,M)
  
  # Combine with original data
  Hat_New_X = New_X * M + G_sample * (1-M)
  
  # Discriminator
  D_prob = discriminator(Hat_New_X, H)
  
  #%% Loss
  D_loss1 = -tf.reduce_mean(M * tf.log(D_prob + 1e-8) + (1-M) * tf.log(1. - D_prob + 1e-8)) 
  G_loss1 = -tf.reduce_mean((1-M) * tf.log(D_prob + 1e-8))
  MSE_train_loss = tf.reduce_mean((M * New_X - M * G_sample)**2) / tf.reduce_mean(M)
  
  D_loss = D_loss1
  G_loss = G_loss1 + alpha * MSE_train_loss 
  
  #%% MSE Performance metric
  MSE_test_loss = tf.reduce_mean(((1-M) * X - (1-M)*G_sample)**2) / tf.reduce_mean(1-M)
  
  #%% Solver
  D_solver = tf.train.AdamOptimizer().minimize(D_loss, var_list=theta_D)
  G_solver = tf.train.AdamOptimizer().minimize(G_loss, var_list=theta_G)
  
  # Sessions
  sess = tf.Session()
  sess.run(tf.global_variables_initializer())
  
  #%% Iterations
  
  #%% Start Iterations
  for it in tqdm(range(n_epochs)):    
      
      #%% Inputs
      mb_idx = sample_idx(Train_No, mb_size)
      X_mb = trainX[mb_idx,:]  
      
      Z_mb = sample_Z(mb_size, Dim) 
      M_mb = trainM[mb_idx,:]  
      H_mb1 = sample_M(mb_size, Dim, 1-p_hint)
      H_mb = M_mb * H_mb1
      
      New_X_mb = M_mb * X_mb + (1-M_mb) * Z_mb  # Missing Data Introduce
      
      _, D_loss_curr = sess.run([D_solver, D_loss1], feed_dict = {M: M_mb, New_X: New_X_mb, H: H_mb})
      _, G_loss_curr, MSE_train_loss_curr, MSE_test_loss_curr = sess.run([G_solver, G_loss1, MSE_train_loss, MSE_test_loss],
                                                                         feed_dict = {X: X_mb, M: M_mb, New_X: New_X_mb, H: H_mb})
              
          
      #%% Intermediate Losses
      if it % 100 == 0:
          print('Iter: {}'.format(it))
          print('Train_loss: {:.4}'.format(np.sqrt(MSE_train_loss_curr)))
          print('Test_loss: {:.4}'.format(np.sqrt(MSE_test_loss_curr)))
          print()
      
      
  #%% Final Loss
      
  Z_mb = sample_Z(Test_No, Dim)  # noise Unif(0, 0.01)
  M_mb = testM
  X_mb = testX
          
  New_X_mb = M_mb * X_mb + (1-M_mb) * Z_mb  # Missing Data Introduce
      
  MSE_final, Sample = sess.run([MSE_test_loss, G_sample], feed_dict = {X: testX, M: testM, New_X: New_X_mb})
          
  print('Final Test RMSE: ' + str(np.sqrt(MSE_final)))
  return {'MSE': MSE_final, 'MSE_train': MSE_train_loss_curr, 'MSE_test': MSE_test_loss_curr,'Data': Data, 'X': X_mb, 'M': M_mb, 'New_X': New_X_mb, 'Sample': Sample, 'idx': idx, 'Train_No': Train_No, 'Test_No': Test_No, 'trainX': trainX, 'testX': testX, 'trainM': trainM, 'testM': testM}

#################################################################################################

## NIMIWAE (MNAR Selection Q1 model)
# covars_r: numpy vector of 0's and 1's, length ncol(data). Indicator of whether feature is used as covariate in modelling missingness R
# def run_NIMIWAE(data,Missing,covars_r,norm_means,norm_sds,train=1,saved_model=None,h=128,sigma="relu",bs = 64,n_epochs = 501,lr=0.001,niw=20,dim_z=10,L=100):
#   # L: number of MC samples used in imputation
#   import torch
#   #import torchvision
#   import torch.nn as nn
#   import numpy as np
#   import scipy.stats
#   import scipy.io
#   import scipy.sparse
#   from scipy.io import loadmat
#   import pandas as pd
#   import matplotlib.pyplot as plt
#   import torch.distributions as td
#   from torch import nn, optim
#   from torch.nn import functional as F
#   #from torchvision import datasets, transforms
#   #from torchvision.utils import save_image
#   import time
#   def mse(xhat,xtrue,mask): # MSE function for imputations
#     xhat = np.array(xhat)
#     xtrue = np.array(xtrue)
#     return np.mean(np.power(xhat-xtrue,2)[~mask])
#   
#   # xfull = (data - np.mean(data,0))/np.std(data,0)
#   xfull = (data - norm_means)/norm_sds
#   
#   # Loading and processing data
#   n = xfull.shape[0] # number of observations
#   p = xfull.shape[1] # number of features
#   np.random.seed(1234)
#   
#   # needed?
#   #print('"data": nrow=' + str(data.shape[0]) + ', ncol=' + str(data.shape[1]))
#   # R=mask**2 # convert TRUE/FALSE mask into 0's and 1's R matrix
#   
#   xmiss = np.copy(xfull)
#   xmiss[Missing==0]=np.nan
#   mask = np.isfinite(xmiss) # binary mask that indicates which values are missing
#   
#   xhat_0 = np.copy(xmiss)
#   xhat_0[np.isnan(xmiss)] = 0
#   
#   d = dim_z # dimension of the latent space
#   K = niw # number of IS during training
# 
#   # vector of indicators for inclusion as covariates for modelling R (later will be user-customizable)
#   # for now: first 10 covariates determine missingness
#   # covars_r = np.repeat(0,data.shape[1])
#   # covars_r[0:10]=1  ########### need to change this, add this as input ####################################################################################
#   pr = np.sum(covars_r).astype(int)
#   
#   # Define decoder/encoder
#   p_z = td.Independent(td.Normal(loc=torch.zeros(d).cuda(),scale=torch.ones(d).cuda()),1)
#   
#   # Functions to calculate nimiwae loss and impute using nimiwae
#   def nimiwae_loss(iota_x,mask):
#     batch_size = iota_x.shape[0]
#     out_encoder = encoder(iota_x)
#     q_zgivenxobs = td.Independent(td.Normal(loc=out_encoder[..., :d],scale=torch.nn.Softplus()(out_encoder[..., d:(2*d)])),1)
#     
#     zgivenx = q_zgivenxobs.rsample([K])
#     zgivenx_flat = zgivenx.reshape([K*batch_size,d])
#     
#     out_decoder_x = decoder_x(zgivenx_flat)
#     all_means_obs_model = out_decoder_x[..., :p]
#     all_scales_obs_model = torch.nn.Softplus()(out_decoder_x[..., p:(2*p)]) + 0.001
#     all_degfreedom_obs_model = torch.nn.Softplus()(out_decoder_x[..., (2*p):(3*p)]) + 3
#     
#     data_flat = torch.Tensor.repeat(iota_x,[K,1]).reshape([-1,1])
#     tiledmask = torch.Tensor.repeat(mask,[K,1])
#     
#     #############################################################################
#     pxgivenz = torch.distributions.StudentT(loc=all_means_obs_model.reshape([-1,1]),scale=all_scales_obs_model.reshape([-1,1]),df=all_degfreedom_obs_model.reshape([-1,1]))
#     xgivenz = pxgivenz.rsample([1]) # samples all observed/missing features. sampling once for each of the K samples of z
#     xgivenz_flat = xgivenz.reshape([K*batch_size,p])    # (#iws)*(#bs) x (#features)   --> need to check that order is preserved in dimensions
#     
#     tiled_iota_x = torch.Tensor.repeat(iota_x,[K,1])
#     tiled_miss_samples=xgivenz_flat*(1-tiledmask)
#     xreconstructed = tiled_iota_x+tiled_miss_samples
#     xincluded = xreconstructed[:,covars_r==1]
#     out_decoder_r = decoder_r(xincluded)
#     p_rgivenx = td.Bernoulli(probs=out_decoder_r[..., :p])
#     all_logprgivenx = p_rgivenx.log_prob(tiledmask)
#     logprgivenx = torch.sum(all_logprgivenx,1).reshape([K,batch_size])
#     #############################################################################
#     
#     all_log_pxgivenz_flat = pxgivenz.log_prob(data_flat)
#     all_log_pxgivenz = all_log_pxgivenz_flat.reshape([K*batch_size,p])
#     
#     logpxobsgivenz = torch.sum(all_log_pxgivenz*tiledmask,1).reshape([K,batch_size])
#     logpz = p_z.log_prob(zgivenx)
#     logq = q_zgivenxobs.log_prob(zgivenx)
#     
#     # negative log likelihood. Obtained by taking average across the K MC samples of zgivenxobs
#     #####################################################################################################
#     neg_bound = -torch.mean(torch.logsumexp(logpxobsgivenz + logprgivenx + logpz - logq,0))     ######### Included log p(r|x)
#     #####################################################################################################
#     return neg_bound
#   
#   def nimiwae_impute(iota_x,mask,L):
#     batch_size = iota_x.shape[0]
#     out_encoder = encoder(iota_x)
#     q_zgivenxobs = td.Independent(td.Normal(loc=out_encoder[..., :d],scale=torch.nn.Softplus()(out_encoder[..., d:(2*d)])),1)
#     
#     zgivenx = q_zgivenxobs.rsample([L])
#     zgivenx_flat = zgivenx.reshape([L*batch_size,d])
#     
#     out_decoder_x = decoder_x(zgivenx_flat)
#     all_means_obs_model = out_decoder_x[..., :p]
#     all_scales_obs_model = torch.nn.Softplus()(out_decoder_x[..., p:(2*p)]) + 0.001
#     all_degfreedom_obs_model = torch.nn.Softplus()(out_decoder_x[..., (2*p):(3*p)]) + 3  # for student's T
#     
#     data_flat = torch.Tensor.repeat(iota_x,[L,1]).reshape([-1,1]).cuda()
#     tiledmask = torch.Tensor.repeat(mask,[L,1]).cuda()
#     
#     pxgivenz = torch.distributions.StudentT(loc=all_means_obs_model.reshape([-1,1]),scale=all_scales_obs_model.reshape([-1,1]),df=all_degfreedom_obs_model.reshape([-1,1]))
#     #xgivenz = pxgivenz.rsample([1])
#     
#     all_log_pxgivenz_flat = pxgivenz.log_prob(data_flat)
#     all_log_pxgivenz = all_log_pxgivenz_flat.reshape([L*batch_size,p])
#     
#     logpxobsgivenz = torch.sum(all_log_pxgivenz*tiledmask,1).reshape([L,batch_size])
#     logpz = p_z.log_prob(zgivenx)
#     logq = q_zgivenxobs.log_prob(zgivenx)
#     # logprgivenx
#     
#     xgivenz = td.Independent(td.StudentT(loc=all_means_obs_model, scale=all_scales_obs_model, df=all_degfreedom_obs_model),1)
#     
#     imp_weights = torch.nn.functional.softmax(logpxobsgivenz + logpz - logq,0) # these are w_1,....,w_L for all observations in the batch
#     xms = xgivenz.sample().reshape([L,batch_size,p])
#     xm=torch.einsum('ki,kij->ij', imp_weights, xms) 
#     return {'xm': xm, 'out_encoder': out_encoder,'imp_weights': imp_weights,'zgivenx_flat': zgivenx_flat}
#   
#   # initialize weights
#   def weights_init(layer):
#     if type(layer) == nn.Linear: torch.nn.init.orthogonal_(layer.weight)
#   
#   if train==1:
#     decoder_x = nn.Sequential(
#       torch.nn.Linear(d, h),
#       torch.nn.ReLU(),
#       torch.nn.Linear(h, h),
#       torch.nn.ReLU(),
#       torch.nn.Linear(h, 3*p),  # the decoder will output both the mean, the scale, and the number of degrees of freedoms (hence the 3*p)
#     )
#     decoder_r = nn.Sequential(
#       torch.nn.Linear(pr, h),    # pr: number of covariates that determine missingness mask R
#       torch.nn.ReLU(),
#       torch.nn.Linear(h, h),
#       torch.nn.ReLU(),
#       torch.nn.Linear(h, p),  # the decoder will output Bernoulli probabilities for each p features
#       torch.nn.Sigmoid(),
#     )
#     encoder = nn.Sequential(
#       torch.nn.Linear(p, h),
#       torch.nn.ReLU(),
#       torch.nn.Linear(h, h),
#       torch.nn.ReLU(),
#       torch.nn.Linear(h, 2*d),  # the encoder will output both the mean and the diagonal covariance
#     )
#     encoder.cuda() # we'll use the GPU
#     decoder_x.cuda()
#     decoder_r.cuda()
#     # Define ADAM optimizer
#     optimizer = optim.Adam(list(encoder.parameters()) + list(decoder_x.parameters()) + list(decoder_r.parameters()),lr=lr)
#     encoder.apply(weights_init)
#     decoder_x.apply(weights_init)
#     decoder_r.apply(weights_init)
#   
#   # Train and impute every 100 epochs
#   nimiwae_loss_train=np.array([])
#   mse_train=np.array([])
#   mse_test=np.array([])
#   mse_train2=np.array([])
#   # bs = 64 # batch size
#   # n_epochs = 1001
#   xhat = np.copy(xhat_0) # This will be out imputed data matrix
#   #rgivenx = np.copy(R)   # this will be out probabilities P(R|X) for each cell being missing
#   
#   time_train=[]
#   time_impute=[]
#   NIMIWAE_LB_epoch=[]
#   if train==1:
#     # Training+Imputing
#     for ep in range(1,n_epochs):
#       perm = np.random.permutation(n) # We use the "random reshuffling" version of SGD
#       batches_data = np.array_split(xhat_0[perm,], n/bs)
#       batches_mask = np.array_split(mask[perm,], n/bs)
#       t0_train=time.time()
#       for it in range(len(batches_data)):
#         optimizer.zero_grad()
#         encoder.zero_grad()
#         decoder_x.zero_grad()
#         decoder_r.zero_grad()
#         b_data = torch.from_numpy(batches_data[it]).float().cuda()
#         b_mask = torch.from_numpy(batches_mask[it]).float().cuda()
#         loss = nimiwae_loss(iota_x = b_data,mask = b_mask)
#         loss.backward()
#         optimizer.step()
#       time_train=np.append(time_train,time.time()-t0_train)
#       NIMIWAE_LB=(-np.log(K)-nimiwae_loss(iota_x = torch.from_numpy(xhat_0).float().cuda(),mask = torch.from_numpy(mask).float().cuda()).cpu().data.numpy())
#       NIMIWAE_LB_epoch=np.append(NIMIWAE_LB_epoch,NIMIWAE_LB)
#       if ep % 100 == 1:
#         print('Epoch %g' %ep)
#         print('NIMIWAE likelihood bound  %g' %NIMIWAE_LB) # Gradient step      
#         
#         ### Now we do the imputation
#         t0_impute=time.time()
#         xhat_fit=nimiwae_impute(iota_x = torch.from_numpy(xhat_0).float().cuda(),mask = torch.from_numpy(mask).float().cuda(),L=L)
#         time_impute=np.append(time_impute,time.time()-t0_impute)
#         
#         xhat[~mask] = xhat_fit['xm'].cpu().data.numpy()[~mask]
#         err = np.array([mse(xhat,xfull,mask)])
#         mse_train = np.append(mse_train,err,axis=0)
#         
#         zgivenx_flat = xhat_fit['zgivenx_flat'].cpu().data.numpy()   # L samples*batch_size x d (d: latent dimension)
#         imp_weights = xhat_fit['imp_weights'].cpu().data.numpy()
#         print('Imputation MSE  %g' %err)
#         print('-----')
#     saved_model={'encoder': encoder, 'decoder_x': decoder_x, 'decoder_r': decoder_r}
#     return {'saved_model': saved_model,'zgivenx_flat': zgivenx_flat,'NIMIWAE_LB_epoch': NIMIWAE_LB_epoch,'time_train': time_train,'time_impute': time_impute,'imp_weights': imp_weights,'MSE': mse_train, 'xhat': xhat, 'mask': mask}
#   else:
#     # validating (hyperparameter values) or testing
#     encoder=saved_model['encoder'].cuda()
#     decoder_x=saved_model['decoder_x'].cuda()
#     decoder_r=saved_model['decoder_r'].cuda()
#     for ep in range(1,n_epochs):
#       #perm = np.random.permutation(n) # We use the "random reshuffling" version of SGD
#       #batches_data = np.array_split(xhat_0[perm,], n/bs)
#       #batches_mask = np.array_split(mask[perm,], n/bs)
#       #for it in range(len(batches_data)):
#       #  optimizer.zero_grad()
#       #  encoder.zero_grad()
#       #  decoder_x.zero_grad()
#       #  decoder_r.zero_grad()
#       #  b_data = torch.from_numpy(batches_data[it]).float().cuda()
#       #  b_mask = torch.from_numpy(batches_mask[it]).float().cuda()
#       #  loss = nimiwae_loss(iota_x = b_data,mask = b_mask)
#       #  loss.backward()
#       #  optimizer.step()
#       #time_train=np.append(time_train,time.time()-t0_train)
#       NIMIWAE_LB=(-np.log(K)-nimiwae_loss(iota_x = torch.from_numpy(xhat_0).float().cuda(),mask = torch.from_numpy(mask).float().cuda()).cpu().data.numpy())
#       print('Epoch %g' %ep)
#       print('NIMIWAE likelihood bound  %g' %NIMIWAE_LB) # Gradient step      
#       ### Now we do the imputation
#       t0_impute=time.time()
#       xhat_fit=nimiwae_impute(iota_x = torch.from_numpy(xhat_0).float().cuda(),mask = torch.from_numpy(mask).float().cuda(),L=L)
#       time_impute=np.append(time_impute,time.time()-t0_impute)
#       xhat[~mask] = xhat_fit['xm'].cpu().data.numpy()[~mask]
#       err = np.array([mse(xhat,xfull,mask)])
#       mse_test = np.append(mse_test,err,axis=0)
#       zgivenx_flat = xhat_fit['zgivenx_flat'].cpu().data.numpy()   # L samples*batch_size x d (d: latent dimension)
#       imp_weights = xhat_fit['imp_weights'].cpu().data.numpy()
#       print('Imputation MSE  %g' %err)
#       print('-----')
#     return {'zgivenx_flat': zgivenx_flat,'NIMIWAE_LB': NIMIWAE_LB,'time_impute': time_impute,'imp_weights': imp_weights,'MSE': mse_test, 'xhat': xhat, 'mask': mask}

def run_missForest(data,Missing,maxits=20,n_estimators=20):
  import numpy as np
  from sklearn.ensemble import ExtraTreesRegressor
  from sklearn.experimental import enable_iterative_imputer
  from sklearn.impute import IterativeImputer
  import time
  def mse(xhat,xtrue,mask): # MSE function for imputations
    xhat = np.array(xhat)
    xtrue = np.array(xtrue)
    return np.mean(np.power(xhat-xtrue,2)[~mask])
  
  xfull = (data - np.mean(data,0))/np.std(data,0)
  np.random.seed(1234)
  xmiss = np.copy(xfull)
  xmiss[Missing==0]=np.nan
  mask = np.isfinite(xmiss) # binary mask that indicates which values are missing
  
  t0_mf=time.time()
  missforest = IterativeImputer(max_iter=maxits, estimator=ExtraTreesRegressor(n_estimators=n_estimators))
  missforest.fit(xmiss)
  xhat_mf = missforest.transform(xmiss)
  time_mf=time.time()-t0_mf
  return{'time_mf': time_mf, 'MSE_mf': mse(xhat_mf,xfull,mask), 'xhat_mf': xhat_mf}

def run_meanImputation(data,Missing,maxits=None,n_estimators=None):
  import numpy as np
  from sklearn.impute import SimpleImputer
  import time
  def mse(xhat,xtrue,mask): # MSE function for imputations
    xhat = np.array(xhat)
    xtrue = np.array(xtrue)
    return np.mean(np.power(xhat-xtrue,2)[~mask])

  xfull = (data - np.mean(data,0))/np.std(data,0)
  np.random.seed(1234)
  xmiss = np.copy(xfull)
  xmiss[Missing==0]=np.nan
  mask = np.isfinite(xmiss) # binary mask that indicates which values are missing
  
  t0_mean=time.time()
  mean_imp = SimpleImputer(missing_values=np.nan, strategy='mean')
  mean_imp.fit(xmiss)
  xhat_mean = mean_imp.transform(xmiss)
  time_mean=time.time()-t0_mean
  return{'time_mean': time_mean, 'MSE_mean': mse(xhat_mean,xfull,mask), 'xhat_mean': xhat_mean}

#################################################################################################

## HIVAE (Nazabal et al 2018) (main_scripts.py: uses function from read_functions.py):
# data_types: 
# bs: batch size, n_epochs: #epochs, train: training model flag, display: display trace every # epochs,
# n_save: save variables every n_save iters, restore: restore session
# restore: restore saved result, dim_latent_s-y: Dim of categorical/Z latent/Y latent space, dim_latent_y_partition: partition of Y latent space
# save_file: input string of 
# model_name: 'model_HIVAE_factorized' or 'model_HIVAE_inputDropout' (default)
def run_HIVAE(data,Missing,data_types,lr=1e-3,bs=200,n_epochs=500,train=1, display=100, n_save=1000, restore=0, dim_latent_s=10, dim_latent_z=2, dim_latent_y=10, dim_latent_y_partition=[], model_name="model_HIVAE_inputDropout",save_file="test"):
  import os
  import sys
  import argparse
  import tensorflow as tf
  import time
  import numpy as np
  import csv
  
  import Python.HIVAE.read_functions as read_functions
  import Python.HIVAE.graph_new as graph_new
  # import parser_arguments

  def print_loss(epoch, start_time, avg_loss, avg_test_loglik, avg_KL_s, avg_KL_z):
      print("Epoch: [%2d]  time: %4.4f, train_loglik: %.8f, KL_z: %.8f, KL_s: %.8f, ELBO: %.8f, Test_loglik: %.8f"
            % (epoch, time.time() - start_time, avg_loss, avg_KL_z, avg_KL_s, avg_loss-avg_KL_z-avg_KL_s, avg_test_loglik))

  #Create a directoy for the save file
  if not os.path.exists('./HIVAE_Saved_Networks/' + save_file):
      os.makedirs('./HIVAE_Saved_Networks/' + save_file)

  network_file_name='./HIVAE_Saved_Networks/' + save_file + '/' + save_file +'.ckpt'
  log_file_name='./HIVAE_Saved_Network/' + save_file + '/log_file_' + save_file +'.txt'

  train_data, types_dict, miss_mask, true_miss_mask, n_samples = read_functions.read_data(data, Missing, data_types)
  #Check batch size
  if bs > n_samples:
      bs = n_samples
  #Get an integer number of batches
  n_batches = int(np.floor(np.shape(train_data)[0]/bs))
  #Compute the real miss_mask
  miss_mask = np.multiply(miss_mask, true_miss_mask)

  #Creating graph
  sess_HVAE = tf.Graph()

  with sess_HVAE.as_default():
      tf_nodes = graph_new.HVAE_graph(model_name, data_types, bs,
                                  learning_rate=lr, z_dim=dim_latent_z, y_dim=dim_latent_y, s_dim=dim_latent_s, y_dim_partition=dim_latent_y_partition)

  ################### Running the VAE Training #################################

  with tf.Session(graph=sess_HVAE) as session:

      saver = tf.train.Saver()
      if(restore == 1):
          saver.restore(session, network_file_name)
          print("Model restored.")
      else:
          print('Initizalizing Variables ...')
          tf.global_variables_initializer().run()

      print('Training the HVAE ...')
      if(train == 1):
        
          start_time = time.time()
          # Training cycle
          loglik_epoch = []
          testloglik_epoch = []
          error_train_mode_global = []
          error_test_mode_global = []
          KL_s_epoch = []
          KL_z_epoch = []
          
          time_train=np.array([])
          time_impute=np.array([])
          time_rest=np.array([])
          ELBOs=np.array([])
          
          for epoch in range(n_epochs):
              avg_loss = 0.
              avg_KL_s = 0.
              avg_KL_z = 0.
              samples_list = []
              p_params_list = []
              q_params_list = []
              log_p_x_total = []
              log_p_x_missing_total = []

              # Annealing of Gumbel-Softmax parameter
              tau = np.max([1.0 - 0.01*epoch,1e-3])
              # tau = 1e-3
              tau2 = np.min([0.001*epoch,1.0])

              #Randomize the data in the mini-batches
              random_perm = np.random.permutation(range(np.shape(train_data)[0]))
              train_data_aux = train_data[random_perm,:]
              miss_mask_aux = miss_mask[random_perm,:]
              true_miss_mask_aux = true_miss_mask[random_perm,:]
              
              t_train=np.array([])
              t_impute=np.array([])
              for i in range(n_batches):

                  #Create inputs for the feed_dict
                  data_list, miss_list = read_functions.next_batch(train_data_aux, types_dict, miss_mask_aux, bs, index_batch=i)
                  #Delete not known data (input zeros)
                  data_list_observed = [data_list[i]*np.reshape(miss_list[:,i],[bs,1]) for i in range(len(data_list))]
                  #Create feed dictionary
                  feedDict = {i: d for i, d in zip(tf_nodes['ground_batch'], data_list)}
                  feedDict.update({i: d for i, d in zip(tf_nodes['ground_batch_observed'], data_list_observed)})
                  feedDict[tf_nodes['miss_list']] = miss_list
                  feedDict[tf_nodes['tau_GS']] = tau
                  feedDict[tf_nodes['tau_var']] = tau2

                  t0_train=time.time()
                  #Running VAE
                  _,loss,KL_z,KL_s,samples,log_p_x,log_p_x_missing,p_params,q_params  = session.run([tf_nodes['optim'], tf_nodes['loss_re'], tf_nodes['KL_z'], tf_nodes['KL_s'], tf_nodes['samples'],
                                                           tf_nodes['log_p_x'], tf_nodes['log_p_x_missing'],tf_nodes['p_params'],tf_nodes['q_params']],
                                                           feed_dict=feedDict)
                  t_train=np.append(t_train,time.time()-t0_train)
                  
                  t0_impute=time.time()
                  samples_test,log_p_x_test,log_p_x_missing_test,test_params  = session.run([tf_nodes['samples_test'],tf_nodes['log_p_x_test'],tf_nodes['log_p_x_missing_test'],tf_nodes['test_params']],
                                                               feed_dict=feedDict)
                  t_impute=np.append(t_impute,time.time()-t0_impute)

                  #Evaluate results on the imputation with mode, not on the samlpes!
                  samples_list.append(samples_test)
                  p_params_list.append(test_params)
                  #p_params_list.append(p_params)
                  q_params_list.append(q_params)
                  log_p_x_total.append(log_p_x_test)
                  log_p_x_missing_total.append(log_p_x_missing_test)

                  # Compute average loss
                  avg_loss += np.mean(loss)
                  avg_KL_s += np.mean(KL_s)
                  avg_KL_z += np.mean(KL_z)
                  
              time_train=np.append(time_train,np.sum(t_train))
              time_impute=np.append(time_impute,np.sum(t_impute))

              t0_rest=time.time()
              
              #Concatenate samples in arrays
              s_total, z_total, y_total, est_data = read_functions.samples_concatenation(samples_list)

              #Transform discrete variables back to the original values
              train_data_transformed = read_functions.discrete_variables_transformation(train_data_aux[:n_batches*bs,:], types_dict)
              est_data_transformed = read_functions.discrete_variables_transformation(est_data, types_dict)
              est_data_imputed = read_functions.mean_imputation(train_data_transformed, miss_mask_aux[:n_batches*bs,:], types_dict)

              #est_data_transformed[np.isinf(est_data_transformed)] = 1e20

              #Create global dictionary of the distribution parameters
              p_params_complete = read_functions.p_distribution_params_concatenation(p_params_list, types_dict, dim_latent_z, dim_latent_s)
              q_params_complete = read_functions.q_distribution_params_concatenation(q_params_list,  dim_latent_z, dim_latent_s)

              #Number of clusters created
              cluster_index = np.argmax(q_params_complete['s'],1)
              cluster = np.unique(cluster_index)
              print('Clusters: ' + str(len(cluster)))

              #Compute mean and mode of our loglik models
              loglik_mean, loglik_mode = read_functions.statistics(p_params_complete['x'],types_dict)
              #loglik_mean[np.isinf(loglik_mean)] = 1e20

              #Try this for the errors
              error_train_mean, error_test_mean = read_functions.error_computation(train_data_transformed, loglik_mean, types_dict, miss_mask_aux[:n_batches*bs,:])
              error_train_mode, error_test_mode = read_functions.error_computation(train_data_transformed, loglik_mode, types_dict, miss_mask_aux[:n_batches*bs,:])
              error_train_samples, error_test_samples = read_functions.error_computation(train_data_transformed, est_data_transformed, types_dict, miss_mask_aux[:n_batches*bs,:])
              error_train_imputed, error_test_imputed = read_functions.error_computation(train_data_transformed, est_data_imputed, types_dict, miss_mask_aux[:n_batches*bs,:])

              #Compute test-loglik from log_p_x_missing
              log_p_x_total = np.transpose(np.concatenate(log_p_x_total,1))
              log_p_x_missing_total = np.transpose(np.concatenate(log_p_x_missing_total,1))
              avg_test_loglik = np.sum(log_p_x_missing_total)/(np.sum(1.0-miss_mask_aux)+1e-100)

              # Display logs per epoch step
              if epoch % display == 0:
                  print_loss(epoch, start_time, avg_loss/n_batches, avg_test_loglik, avg_KL_s/n_batches, avg_KL_z/n_batches)
                  print('Test error mode: ' + str(np.round(np.mean(error_test_mode),3)))
                  print("")

              #Compute train and test loglik per variables
              loglik_per_variable = np.sum(log_p_x_total,0)/(np.sum(miss_mask_aux,0)+1e-100) # add very small value --> avoid NaNs
              loglik_per_variable_missing = np.sum(log_p_x_missing_total,0)/(np.sum(1.0-miss_mask_aux,0)+1e-100)

              #Store evolution of all the terms in the ELBO
              loglik_epoch.append(loglik_per_variable)
              testloglik_epoch.append(loglik_per_variable_missing)
              KL_s_epoch.append(avg_KL_s/n_batches)
              KL_z_epoch.append(avg_KL_z/n_batches)
              error_train_mode_global.append(error_train_mode)
              error_test_mode_global.append(error_test_mode)
              LB = (avg_loss/n_batches)-(avg_KL_z/n_batches)-(avg_KL_s/n_batches)
              ELBOs=np.append(ELBOs,LB)


              if epoch % n_save == 0:
                  print('Saving Variables ...')
                  save_path = saver.save(session, network_file_name)
                  
              time_rest=np.append(time_rest,time.time()-t0_rest)


          print('Training Finished ...')
          end_time=time.time()
          time_HIVAE_train=end_time-start_time

          #Saving needed variables in csv
          if not os.path.exists('./HIVAE_Results_csv/' + save_file):
              os.makedirs('./HIVAE_Results_csv/' + save_file)

          with open('HIVAE_Results_csv/' + save_file + '/' + save_file + '_loglik.csv', "w") as f:
              writer = csv.writer(f)
              writer.writerows(loglik_epoch)

          with open('HIVAE_Results_csv/' + save_file + '/' + save_file + '_testloglik.csv', "w") as f:
              writer = csv.writer(f)
              writer.writerows(testloglik_epoch)

          with open('HIVAE_Results_csv/' + save_file + '/' + save_file + '_KL_s.csv', "w") as f:
              writer = csv.writer(f)
              writer.writerows(np.reshape(KL_s_epoch,[-1,1]))

          with open('HIVAE_Results_csv/' + save_file + '/' + save_file + '_KL_z.csv', "w") as f:
              writer = csv.writer(f)
              writer.writerows(np.reshape(KL_z_epoch,[-1,1]))

          with open('HIVAE_Results_csv/' + save_file + '/' + save_file + '_train_error.csv', "w") as f:
              writer = csv.writer(f)
              writer.writerows(error_train_mode_global)

          with open('HIVAE_Results_csv/' + save_file + '/' + save_file + '_test_error.csv', "w") as f:
              writer = csv.writer(f)
              writer.writerows(error_test_mode_global)

          # Save the variables to disk at the end
          save_path = saver.save(session, network_file_name)
          
          train_params = {'lr': lr, 'bs': bs, 'n_epochs': n_epochs, 'dim_latent_s': dim_latent_s, 'dim_latent_z': dim_latent_z, 'dim_latent_y': dim_latent_y, 'model_name': model_name}
          return {'train_params': train_params, 'ELBOs': ELBOs,'time_train':time_train,'time_impute':time_impute,'time_rest':time_rest,'time': time_HIVAE_train,'samples_list': samples_list,'p_params_list': p_params_list,'q_params_list': q_params_list,'s_total': s_total, 'z_total': z_total, 'y_total': y_total, 'est_data': est_data,'ll': loglik_epoch, 'test_ll': testloglik_epoch, 'KL_s': KL_s_epoch, 'KL_z': KL_z_epoch, 'err_train_mode_global': error_train_mode_global, 'err_train_mean': error_train_mean, 'err_train_imputed': error_train_imputed, 'err_test_mode_global': error_test_mode_global, 'err_test_mean': error_test_mean, 'err_test_imputed': error_test_imputed, 'train_x_transf': train_data_transformed, 'est_x_transf': est_data_transformed, 'est_x_imputed': est_data_imputed}
        

      #Test phase
      else:
          start_time = time.time()
          # Training cycle

          #f_toy2, ax_toy2 = plt.subplots(4,4,figsize=(8, 8))
          loglik_epoch = []
          testloglik_epoch = []
          error_train_mode_global = []
          error_test_mode_global = []
          error_imputed_global = []
          est_data_transformed_total = []

          #Only one epoch needed, since we are doing mode imputation
          for epoch in range(n_epochs):
              avg_loss = 0.
              avg_KL_s = 0.
              avg_KL_y = 0.
              avg_KL_z = 0.
              samples_list = []
              p_params_list = []
              q_params_list = []
              log_p_x_total = []
              log_p_x_missing_total = []

              label_ind = 2

              # Constant Gumbel-Softmax parameter (where we have finished the annealing)
              tau = 1e-3
              #tau = 1.0

              # Randomize the data in the mini-batches
              #random_perm = np.random.permutation(range(np.shape(train_data)[0]))
              # random_perm = range(np.shape(train_data)[0])
              random_perm = range(n_batches*bs)
              train_data_aux = train_data[random_perm,:]
              miss_mask_aux = miss_mask[random_perm,:]
              true_miss_mask_aux = true_miss_mask[random_perm,:]

              for i in range(n_batches):

                  #Create train minibatch
                  data_list, miss_list = read_functions.next_batch(train_data_aux, types_dict, miss_mask_aux, bs,
                                                                   index_batch=i)
                  #print(np.mean(data_list[0],0))

                  #Delete not known data
                  data_list_observed = [data_list[i]*np.reshape(miss_list[:,i],[bs,1]) for i in range(len(data_list))]


                  #Create feed dictionary
                  feedDict = {i: d for i, d in zip(tf_nodes['ground_batch'], data_list)}
                  feedDict.update({i: d for i, d in zip(tf_nodes['ground_batch_observed'], data_list_observed)})
                  feedDict[tf_nodes['miss_list']] = miss_list
                  feedDict[tf_nodes['tau_GS']] = tau

                  #Get samples from the model
                  loss,KL_z,KL_s,samples,log_p_x,log_p_x_missing,p_params,q_params  = session.run([tf_nodes['loss_re'], tf_nodes['KL_z'], tf_nodes['KL_s'], tf_nodes['samples'],
                                                               tf_nodes['log_p_x'], tf_nodes['log_p_x_missing'],tf_nodes['p_params'],tf_nodes['q_params']],
                                                               feed_dict=feedDict)

                  samples_test,log_p_x_test,log_p_x_missing_test,test_params  = session.run([tf_nodes['samples_test'],tf_nodes['log_p_x_test'],tf_nodes['log_p_x_missing_test'],tf_nodes['test_params']],
                                                               feed_dict=feedDict)


                  samples_list.append(samples_test)
                  p_params_list.append(test_params)
                  q_params_list.append(q_params)
                  log_p_x_total.append(log_p_x_test)
                  log_p_x_missing_total.append(log_p_x_missing_test)

                  # Compute average loss
                  avg_loss += np.mean(loss)
                  avg_KL_s += np.mean(KL_s)
                  avg_KL_z += np.mean(KL_z)

              #Separate the samples from the batch list
              s_aux, z_aux, y_total, est_data = read_functions.samples_concatenation(samples_list)

              #Transform discrete variables to original values
              train_data_transformed = read_functions.discrete_variables_transformation(train_data_aux[:n_batches*bs,:], types_dict)
              est_data_transformed = read_functions.discrete_variables_transformation(est_data, types_dict)
              est_data_imputed = read_functions.mean_imputation(train_data_transformed, miss_mask_aux[:n_batches*bs,:], types_dict)

              #Create global dictionary of the distribution parameters
              p_params_complete = read_functions.p_distribution_params_concatenation(p_params_list, types_dict, dim_latent_z, dim_latent_s)
              q_params_complete = read_functions.q_distribution_params_concatenation(q_params_list,  dim_latent_z, dim_latent_s)

              #Number of clusters created
              cluster_index = np.argmax(q_params_complete['s'],1)
              cluster = np.unique(cluster_index)
              print('Clusters: ' + str(len(cluster)))

              #Compute mean and mode of our loglik models
              loglik_mean, loglik_mode = read_functions.statistics(p_params_complete['x'],types_dict)

              #Try this for the errors
              error_train_mean, error_test_mean = read_functions.error_computation(train_data_transformed, loglik_mean, types_dict, miss_mask_aux[:n_batches*bs,:])
              error_train_mode, error_test_mode = read_functions.error_computation(train_data_transformed, loglik_mode, types_dict, miss_mask_aux[:n_batches*bs,:])
              error_train_samples, error_test_samples = read_functions.error_computation(train_data_transformed, est_data_transformed, types_dict, miss_mask_aux[:n_batches*bs,:])
              error_train_imputed, error_test_imputed = read_functions.error_computation(train_data_transformed, est_data_imputed, types_dict, miss_mask_aux[:n_batches*bs,:])

              # Compute test-loglik from log_p_x_missing
              log_p_x_missing_total = np.transpose(np.concatenate(log_p_x_missing_total,1))
              avg_test_loglik = np.sum(log_p_x_missing_total)/(np.sum(1.0-miss_mask_aux)+1e-100)

              # Display logs per epoch step
              if epoch % display == 0:
                  print(np.round(error_test_mode,3))
                  print('Test error mode: ' + str(np.round(np.mean(error_test_mode),3)))
                  print("")

              #Plot evolution of test loglik
              loglik_per_variable = np.sum(np.concatenate(log_p_x_total,1),1)/(np.sum(miss_mask,0)+1e-100)
              loglik_per_variable_missing = np.sum(log_p_x_missing_total,0)/(np.sum(1.0-miss_mask,0)+1e-100)

              loglik_epoch.append(loglik_per_variable)
              testloglik_epoch.append(loglik_per_variable_missing)

              print('Test loglik: ' + str(np.round(np.mean(loglik_per_variable_missing),3)))


              #Re-run test error mode
              error_train_mode_global.append(error_train_mode)
              error_test_mode_global.append(error_test_mode)
              error_imputed_global.append(error_test_imputed)

              #Store data samples
              est_data_transformed_total.append(est_data_transformed)

          #Compute the data reconstruction
          data_reconstruction = train_data_transformed * miss_mask_aux[:n_batches*bs,:] + \
                                      np.round(loglik_mode,3) * (1 - miss_mask_aux[:n_batches*bs,:])
    
          train_data_transformed = train_data_transformed[np.argsort(random_perm)]
          data_reconstruction = data_reconstruction[np.argsort(random_perm)]
          
          end_time=time.time()
          time_HIVAE_test=end_time-start_time


          if not os.path.exists('./HIVAE_Results/' + save_file):
              os.makedirs('./HIVAE_Results/' + save_file)

          with open('HIVAE_Results/' + save_file + '/' + save_file + '_data_reconstruction.csv', "w") as f:
              writer = csv.writer(f)
              writer.writerows(data_reconstruction)
          with open('HIVAE_Results/' + save_file + '/' + save_file + '_data_true.csv', "w") as f:
              writer = csv.writer(f)
              writer.writerows(train_data_transformed)


          #Saving needed variables in csv
          if not os.path.exists('./HIVAE_Results_test_csv/' + save_file):
              os.makedirs('./HIVAE_Results_test_csv/' + save_file)

          #Train loglik per variable
          with open('HIVAE_Results_test_csv/' + save_file + '/' + save_file + '_loglik.csv', "w") as f:
              writer = csv.writer(f)
              writer.writerows(loglik_epoch)

          #Test loglik per variable
          with open('HIVAE_Results_test_csv/' + save_file + '/' + save_file + '_testloglik.csv', "w") as f:
              writer = csv.writer(f)
              writer.writerows(testloglik_epoch)

          #Train NRMSE per variable
          with open('HIVAE_Results_test_csv/' + save_file + '/' + save_file + '_train_error.csv', "w") as f:
              writer = csv.writer(f)
              writer.writerows(error_train_mode_global)

          #Test NRMSE per variable
          with open('HIVAE_Results_test_csv/' + save_file + '/' + save_file + '_test_error.csv', "w") as f:
              writer = csv.writer(f)
              writer.writerows(error_test_mode_global)

          #Number of clusters
          with open('HIVAE_Results_test_csv/' + save_file + '/' + save_file + '_clusters.csv', "w") as f:
              writer = csv.writer(f)
              writer.writerows([[len(cluster)]])
              
          return {'time': time_HIVAE_test, 'cluster': cluster,'samples_list': samples_list,'p_params_list': p_params_list,'q_params_list': q_params_list, 's_aux': s_aux, 'z_aux': z_aux, 'y_total': y_total, 'est_data': est_data,'data_reconstructed': data_reconstruction, 'train_data_transformed': train_data_transformed, 'll': loglik_epoch, 'test_ll': testloglik_epoch, 'err_train_mode_global': error_train_mode_global, 'err_train_mean': error_train_mean, 'err_train_imputed': error_train_imputed, 'err_test_mode_global': error_test_mode_global, 'err_test_mean': error_test_mean, 'err_test_imputed': error_test_imputed, 'err_imp_global': error_imputed_global, 'mean_loss': avg_loss, 'mean_KL_s': avg_KL_s, 'mean_KL_z': avg_KL_z}

#################################################################################################

## VAEAC (Ivanov et al 2019)
# one_hot_max_sizes (list: c(ints)): The space-separated list of one-hot max sizes for categorical features and 
# 0 or 1 for real-valued ones. A categorical feature is supposed to be a column of integers 
# from 0 to K-1, where K is one-hot max size for the feature. The length of the list
# must be equal to the number of columns in the data
# validation_ratio: proportion of samples to include in validation set
# validations_per_epoch: integer number of IWAE estimations on the validation set per one epoch on the training set
# validation_iwae_n_samples: number of samples per object to estimate IWAE on the validation set
# restore: True/False for restoring previously trained VAE
# output_file: file name where you want to save results (tsv)
def run_VAEAC(data,one_hot_max_sizes,norm_mean,norm_std,h,n_hidden_layers,dim_z,bs,lr,output_file,train=1,saved_model=None,saved_networks=None,n_epochs=500,n_imputations=5,validation_ratio=0.2,validations_per_epoch=1,validation_iwae_n_samples=25,restore=False):
  from argparse import ArgumentParser
  from copy import deepcopy
  from importlib import import_module
  from math import ceil
  from os.path import exists, join
  from sys import stderr

  import numpy as np
  import torch
  from torch.utils.data import DataLoader
  from tqdm import tqdm
  import os
  
  #from datasets import compute_normalization    # did it manually in R --> fed in
  from Python.vaeac.imputation_networks import get_imputation_networks
  from Python.vaeac.train_utils import extend_batch, get_validation_iwae
  from Python.vaeac.VAEAC import VAEAC
  import time
  
  import warnings
  warnings.simplefilter("ignore") # ignore the deprecation warnings

  # Read and normalize input data
  raw_data = torch.from_numpy(data).float()
  # norm_mean, norm_std = compute_normalization(raw_data, one_hot_max_sizes)
  
  norm_mean = torch.from_numpy(norm_mean).float()
  norm_std = torch.max(torch.from_numpy(norm_std).float(), torch.tensor(1e-9))
  
  # print(raw_data.shape)
  # print(norm_mean.shape)
  # print(norm_std.shape)
  data = (raw_data - norm_mean[None]) / norm_std[None]
  
  # Default parameters which are not supposed to be changed from user interface
  use_cuda = torch.cuda.is_available()
  verbose = True
  # Non-zero number of workers cause nasty warnings because of some bug in
  # multiprocess library. It might be fixed now, but anyway there is no need
  # to have a lot of workers for dataloader over in-memory tabular data.
  num_workers = 0
  
  if train==1:
    # design all necessary networks and learning parameters for the dataset
    networks = get_imputation_networks(one_hot_max_sizes,h,n_hidden_layers,dim_z,bs,lr)
  
    # build VAEAC on top of returned network, optimizer on top of VAEAC,
    # extract optimization parameters and mask generator
    model = VAEAC(
        networks['reconstruction_log_prob'],
        networks['proposal_network'],
        networks['prior_network'],
        networks['generative_network']
    )
    if use_cuda:
        model = model.cuda()
    optimizer = networks['optimizer'](model.parameters())
    batch_size = networks['batch_size']
    mask_generator = networks['mask_generator']
    vlb_scale_factor = networks.get('vlb_scale_factor', 1)
    
    # train-validation split
    val_size = ceil(len(data) * validation_ratio)
    val_indices = np.random.choice(len(data), val_size, False)
    val_indices_set = set(val_indices)
    train_indices = [i for i in range(len(data)) if i not in val_indices_set]
    train_data = data[train_indices]
    val_data = data[val_indices]
    
    # initialize dataloaders
    dataloader = DataLoader(train_data, batch_size=batch_size, shuffle=True,
                            num_workers=num_workers, drop_last=False)
    val_dataloader = DataLoader(val_data, batch_size=batch_size, shuffle=True,
                                num_workers=num_workers, drop_last=False)
    
    # number of batches after which it is time to do validation
    validation_batches = ceil(len(dataloader) / validations_per_epoch)
    
    # a list of validation IWAE estimates
    validation_iwae = []
    # a list of running variational lower bounds on the train set
    train_vlb = []
    # the length of two lists above is the same because the new
    # values are inserted into them at the validation checkpoints only
    
    # best model state according to the validation IWAE
    best_state = None
    
    # main train loop
    for epoch in range(n_epochs):
    
        iterator = dataloader
        avg_vlb = 0
        if verbose:
            print('Epoch %d...' % (epoch + 1), file=stderr, flush=True)
            iterator = tqdm(iterator)
    
        # one epoch
        for i, batch in enumerate(iterator):
    
            # the time to do a checkpoint is at start and end of the training
            # and after processing validation_batches batches
            if any([
                        i == 0 and epoch == 0,
                        i % validation_batches == validation_batches - 1,
                        i + 1 == len(dataloader)
                    ]):
                val_iwae = get_validation_iwae(val_dataloader, mask_generator,
                                               batch_size, model,
                                               validation_iwae_n_samples,
                                               verbose)
                validation_iwae.append(val_iwae)
                train_vlb.append(avg_vlb)
    
                # if current model validation IWAE is the best validation IWAE
                # over the history of training, the current state
                # is saved to best_state variable
                if max(validation_iwae[::-1]) <= val_iwae:
                    best_state = deepcopy({
                        'epoch': epoch,
                        'model_state_dict': model.state_dict(),
                        'optimizer_state_dict': optimizer.state_dict(),
                        'validation_iwae': validation_iwae,
                        'train_vlb': train_vlb,
                    })
    
                if verbose:
                    print(file=stderr)
                    print(file=stderr)
    
            # if batch size is less than batch_size, extend it with objects
            # from the beginning of the dataset
            batch = extend_batch(batch, dataloader, batch_size)
    
            # generate mask and do an optimizer step over the mask and the batch
            mask = mask_generator(batch)
            optimizer.zero_grad()
            if use_cuda:
                batch = batch.cuda()
                mask = mask.cuda()
            vlb = model.batch_vlb(batch, mask).mean()
            (-vlb / vlb_scale_factor).backward()
            optimizer.step()
    
            # update running variational lower bound average
            avg_vlb += (float(vlb) - avg_vlb) / (i + 1)
            if verbose:
                iterator.set_description('Train VLB: %g' % avg_vlb)
    
    # if use doesn't set use_last_checkpoint flag,
    # use the best model according to the validation IWAE
    if not restore:
        model.load_state_dict(best_state['model_state_dict'])
    
    train_params = {'h':h, 'n_hidden_layers':n_hidden_layers, 'dim_z':dim_z, 'bs':bs, 'lr':lr, 'n_epochs': n_epochs, 'n_imputations':n_imputations, 'validation_ratio':validation_ratio, 'validations_per_epoch':validations_per_epoch,'validation_iwae_n_samples':validation_iwae_n_samples}
    return {'saved_model': model, 'saved_networks': networks,'train_params': train_params, 'LB':avg_vlb,'best_state': best_state}

  elif train==0:
    model = saved_model
    networks = saved_networks
    
    # one epoch through to get avg_vlb
    #optimizer = networks['optimizer'](model.parameters())
    batch_size = networks['batch_size']
    mask_generator = networks['mask_generator']

    # train-validation split
    #val_size = ceil(len(data) * 0)
    #val_indices = np.random.choice(len(data), val_size, False)
    #val_indices_set = set(val_indices)
    #train_indices = [i for i in range(len(data)) if i not in val_indices_set]
    #train_data = data[train_indices]
    train_data = data  ## "Train" data: all data --> just for vlb quantity

    # initialize dataloader
    dataloader = DataLoader(train_data, batch_size=batch_size, shuffle=True,
                            num_workers=num_workers, drop_last=False)
    
    # best model state according to the validation IWAE
    best_state = None
    n_epochs=1
    for epoch in range(n_epochs):
    
        iterator = dataloader
        avg_vlb = 0
        if verbose:
            print('Epoch %d...' % (epoch + 1), file=stderr, flush=True)
            iterator = tqdm(iterator)
    
        # one epoch
        for i, batch in enumerate(iterator):
    
            # if batch size is less than batch_size, extend it with objects
            # from the beginning of the dataset
            batch = extend_batch(batch, dataloader, batch_size)
    
            # generate mask and do an optimizer step over the mask and the batch
            mask = mask_generator(batch)
            if use_cuda:
                batch = batch.cuda()
                mask = mask.cuda()
            vlb = model.batch_vlb(batch, mask).mean()

            # update running variational lower bound average
            avg_vlb += (float(vlb) - avg_vlb) / (i + 1)
            if verbose:
                iterator.set_description('Train VLB: %g' % avg_vlb)
    
  
    # build dataloader for the whole input data
    dataloader = DataLoader(data, batch_size=batch_size,
                            shuffle=False, num_workers=num_workers,
                            drop_last=False)
  
    # prepare the store for the imputations
    results = []
    for i in range(n_imputations):
        results.append([])
    
    iterator = dataloader
    if verbose:
        iterator = tqdm(iterator)
    
    # impute missing values for all input data
    for batch in iterator:
    
        # if batch size is less than batch_size, extend it with objects
        # from the beginning of the dataset
        batch_extended = torch.tensor(batch)
        batch_extended = extend_batch(batch_extended, dataloader, batch_size)
    
        if use_cuda:
            batch = batch.cuda()
            batch_extended = batch_extended.cuda()
    
        # compute the imputation mask
        mask_extended = torch.isnan(batch_extended).float()
    
        # compute imputation distributions parameters
        with torch.no_grad():
            samples_params = model.generate_samples_params(batch_extended,
                                                           mask_extended,
                                                           n_imputations)
            samples_params = samples_params[:batch.shape[0]]
    
        # make a copy of batch with zeroed missing values
        mask = torch.isnan(batch)
        batch_zeroed_nans = torch.tensor(batch)
        batch_zeroed_nans[mask] = 0
    
        # impute samples from the generative distributions into the data
        # and save it to the results
        for i in range(n_imputations):
            sample_params = samples_params[:, i]
            sample = networks['sampler'](sample_params)
            sample[(~mask).byte()] = 0
            sample += batch_zeroed_nans
            results[i].append(torch.tensor(sample, device='cpu'))
    
    # concatenate all batches into one [n x K x D] tensor,
    # where n in the number of objects, K is the number of imputations
    # and D is the dimensionality of one object
    for i in range(len(results)):
        results[i] = torch.cat(results[i]).unsqueeze(1)
    result = torch.cat(results, 1)
    
    # reshape result, undo normalization and save it
    result = result.view(result.shape[0] * result.shape[1], result.shape[2])
    result = result * norm_std[None] + norm_mean[None]
    
    if output_file:
      np.savetxt(output_file, result.numpy(), delimiter='\t')
    if use_cuda:
      sample=sample.detach().cpu().clone().numpy()
      samples_params=samples_params.detach().cpu().clone().numpy()
      result=result.detach().cpu().clone().numpy()
      data=data.detach().cpu().clone().numpy()
    else:
      sample=sample.numpy()
      samples_params=samples_params.numpy()
      result=result.numpy()
      data=data.numpy()
    
    train_params = {'h':h, 'n_hidden_layers':n_hidden_layers, 'dim_z':dim_z, 'bs':bs, 'lr':lr, 'n_epochs': n_epochs, 'n_imputations':n_imputations, 'validation_ratio':validation_ratio, 'validations_per_epoch':validations_per_epoch,'validation_iwae_n_samples':validation_iwae_n_samples}
    return {'train_params': train_params, 'LB':avg_vlb, 'sample': sample,'samples_params': samples_params,'best_model': model,'result': result, 'data': data}

#################################################################################################

## AC-Flow (Li, Akbar, and Oliva 2019)

def run_ACFLOW(data,params):
  import os
  import sys
  p = os.path.split(os.path.dirname(os.path.abspath(__file__)))[0]
  sys.path.append(p)
  import argparse
  import logging
  import tensorflow as tf
  import numpy as np
  from pprint import pformat, pprint
  
  from datasets import get_dataset
  from models import get_model
  from utils.hparams import HParams
  
  ## params as input (list of objects, template json file)
  # parser = argparse.ArgumentParser()
  # parser.add_argument('--cfg_file', type=str)
  # args = parser.parse_args()
  # params = HParams(args.cfg_file)
  pprint(params.dict)
  
  os.environ['CUDA_VISIBLE_DEVICES'] = params.gpu
  np.random.seed(params.seed)
  tf.set_random_seed(params.seed)
  
  ############################################################
  logging.basicConfig(filename=params.exp_dir + '/train.log',
                      filemode='w',
                      level=logging.INFO,
                      format='%(message)s')
  logging.info(pformat(params.dict))
  writer = tf.summary.FileWriter(params.exp_dir + '/summaries')
  ############################################################
  
  trainset = get_dataset('train', params)
  validset = get_dataset('valid', params)
  testset = get_dataset('test', params)
  logging.info(f"trainset: {trainset.size} \
                 validset: {validset.size} \
                 testset: {testset.size}")
  
  model = get_model(params)
  model.build(trainset, validset, testset)
  
  total_params = 0
  trainable_variables = tf.trainable_variables()
  logging.info('=' * 20)
  logging.info("Variables:")
  logging.info(pformat(trainable_variables))
  for k, v in enumerate(trainable_variables):
      num_params = np.prod(v.get_shape().as_list())
      total_params += num_params
  
  logging.info("TOTAL TENSORS: %d TOTAL PARAMS: %f[M]" % (
      k + 1, total_params / 1e6))
  logging.info('=' * 20)
  
  ##########################################################


  def train_epoch(sess):
      train_ll = []
      num_steps = trainset.num_steps
      trainset.initialize(sess)
      for i in range(num_steps):
          if (params.summ_freq > 0) and (i % params.summ_freq == 0):
              ll, summ, step, _ = sess.run(
                  [model.train_ll, model.summ_op,
                   model.global_step, model.train_op])
              writer.add_summary(summ, step)
          else:
              ll, _ = sess.run([model.train_ll, model.train_op])
          train_ll.append(ll)
          if (params.print_freq > 0) and (i % params.print_freq == 0):
              nll = np.mean(-ll)
              logging.info('step: %d nll:%.4f ' % (i, nll))
      train_ll = np.concatenate(train_ll, axis=0)
  
      return np.mean(-train_ll)


  def valid_epoch(sess):
      valid_ll = []
      num_steps = validset.num_steps
      validset.initialize(sess)
      for i in range(num_steps):
          ll = sess.run(model.valid_ll)
          valid_ll.append(ll)
      valid_ll = np.concatenate(valid_ll, axis=0)
  
      return np.mean(-valid_ll)


  def test_epoch(sess):
      test_ll = []
      num_steps = testset.num_steps
      testset.initialize(sess)
      for i in range(num_steps):
          ll = sess.run(model.test_ll)
          test_ll.append(ll)
      test_ll = np.concatenate(test_ll, axis=0)
  
      return np.mean(-test_ll)


  ##########################################################
  initializer = tf.global_variables_initializer()
  saver = tf.train.Saver(tf.global_variables(), max_to_keep=1)
  begin_at_epoch = 0

  config = tf.ConfigProto()
  config.log_device_placement = True
  config.allow_soft_placement = True
  config.gpu_options.allow_growth = True
  sess = tf.Session(config=config)

  sess.run(initializer)
  if params.restore_from != '':
      logging.info('Restoring parameters from %s' % params.restore_from)
      if os.path.isdir(params.restore_from):
          restore_from = tf.train.latest_checkpoint(
              params.restore_from)
          begin_at_epoch = int(restore_from.split('-')[-1])
          saver.restore(sess, restore_from)

  logging.info('starting training')
  best_valid_metric = np.inf
  best_test_metric = np.inf
  for epoch in range(begin_at_epoch, begin_at_epoch + params.epochs):
      train_metric = train_epoch(sess)
      valid_metric = valid_epoch(sess)
      test_metric = test_epoch(sess)
      # save
      if valid_metric < best_valid_metric:
          best_valid_metric = valid_metric
          save_path = os.path.join(params.exp_dir, 'weights', 'epoch')
          saver.save(sess, save_path, global_step=epoch + 1)
      if test_metric < best_test_metric:
          best_test_metric = test_metric
  
      logging.info("Epoch %d, train: %.4f, valid: %.4f/%.4f, test: %.4f/%.4f" %
                   (epoch, train_metric, valid_metric, best_valid_metric,
                    test_metric, best_test_metric))
      sys.stdout.flush()
  return "hello world"

















#################################################################################################
#################################################################################################
#################################################################################################
#################################################################################################
#################################################################################################
#################################################################################################
# Other methods:

## IFAC-VAE (McCoy et al 2018). Handles missing data, but training set must have fully-observed observations to train on

"""
Main file to run VAE for missing data imputation.
Presented at IFAC MMM2018 by JT McCoy, RS Kroon and L Auret.

Based on implementations
of VAEs from:
    https://github.com/twolffpiggott/autoencoders
    https://jmetzen.github.io/2015-11-27/vae.html
    https://github.com/lazyprogrammer/machine_learning_examples/blob/master/unsupervised_class3/vae_tf.py
    https://github.com/deep-learning-indaba/practicals2017/blob/master/practical5.ipynb

VAE is designed to handle real-valued data, not binary data, so the source code
has been adapted to work only with Gaussians as the output of the generative
model (p(x|z)).
"""
# Data: panda df. MissingData: panda df of Data with missingness
# n_epochs: #epochs, lr: learning rate, bs: batch size, dim_latent: latent space dimension
# dim_dec1/dec2/enc1/enc2: # of neurons in each layer
def run_IFACVAE(data,MissingData,train_obs=1,n_epochs=5000,lr=0.001,bs=250,dim_latent=5,n_impute=25,n_batch_test=2000,dim_dec1=20,dim_dec2=20,dim_enc1=20,dim_enc2=20):
  import numpy as np
  from sklearn.preprocessing import StandardScaler
  import matplotlib.pyplot as plt
  # import pandas as pd
  import random
  import os
  os.chdir(os.path.expanduser("~/Research/P2/Python/IFAC-VAE-Imputation"))
  from autoencoders import TFVariationalAutoencoder
  os.chdir(os.path.expanduser("~/Research/P2"))
  
  # LOAD DATA as panda df --> convert to numpy array
  # # select data source:
  # # can be "mill" or "X"
  # data_source = "X"
  # # select corruption level:
  # # can be "light" or "heavy"
  # corr_level = "light"
  # # Path to uncorrupted data:
  # DataPath = data_source + "data.csv"
  # # Path to corrupted data:
  # CorruptDataPath = data_source + "datacorrupt" + corr_level + ".csv"
  
  # # Load data from a csv for analysis:
  # Xdata_df = pd.read_csv(DataPath)
  # Xdata = Xdata_df.values # converts to numpy array
  # del Xdata_df
  # # Load data with missing values from a csv for analysis:
  # Xdata_df = pd.read_csv(CorruptDataPath)
  # Xdata_Missing = Xdata_df.values
  # del Xdata_df
  
  Xdata=data
  Xdata_Missing=MissingData
  
  
  
  # Properties of data:
  Xdata_length = Xdata_Missing.shape[0] # number of data points to use
  n_x = Xdata_Missing.shape[1] # dimensionality of data space
  ObsRowInd = np.where(np.isfinite(np.sum(Xdata_Missing,axis=1)))
  NanRowInd = np.where(np.isnan(np.sum(Xdata_Missing,axis=1)))
  NanIndex = np.where(np.isnan(Xdata_Missing))
  Xdata_Missing_Rows = NanRowInd[0] # number of rows with missing values
  
  # Number of missing values
  NanCount = len(NanIndex[0])
  
  # Zscore for reconstruction error checking:
  scRecon = StandardScaler()
  scRecon.fit(Xdata)
  
  # Zscore of data produces much better results
  sc = StandardScaler()
  # Xdata_Missing_complete = np.copy(Xdata_Missing[ObsRowInd[0],:])
  # standardise using complete records:
  # sc.fit(Xdata_Missing_complete)
  Xdata_Missing[NanIndex] = 0
  sc.fit(Xdata_Missing)
  Xdata_Missing = sc.transform(Xdata_Missing)
  Xdata_Missing[NanIndex] = np.nan
  # del Xdata_Missing_complete
  Xdata = sc.transform(Xdata)
  
  def next_batch(Xdata,batch_size, MissingVals = False):
      """ Randomly sample batch_size elements from the matrix of data, Xdata.
          Xdata is an [NxM] matrix, N observations of M variables.
          batch_size must be smaller than N.
          
          Returns Xdata_sample, a [batch_size x M] matrix.
      """
      if MissingVals:
          # This returns records with any missing values replaced by 0:
          Xdata_length = Xdata.shape[0]
          X_indices = random.sample(range(Xdata_length),batch_size)
          Xdata_sample = np.copy(Xdata[X_indices,:])
          NanIndex = np.where(np.isnan(Xdata_sample))
          Xdata_sample[NanIndex] = 0
      else:
          # This returns complete records only:
          ObsRowIndex = np.where(np.isfinite(np.sum(Xdata,axis=1)))
          X_indices = random.sample(list(ObsRowIndex[0]),batch_size)
          Xdata_sample = np.copy(Xdata[X_indices,:])
      
      return Xdata_sample
  
  '''
  ==============================================================================
  '''
  # INITIALISE AND TRAIN VAE
  # define dict for network structure:
  network_architecture = \
      dict(n_hidden_recog_1=dim_enc1, # 1st layer encoder neurons
           n_hidden_recog_2=dim_enc2, # 2nd layer encoder neurons
           n_hidden_gener_1=dim_dec1, # 1st layer decoder neurons
           n_hidden_gener_2=dim_dec2, # 2nd layer decoder neurons
           n_input=n_x, # data input size
           n_z=dim_latent)  # dimensionality of latent space
  
  # initialise VAE:
  vae = TFVariationalAutoencoder(network_architecture, 
                               learning_rate=lr, 
                               batch_size=bs)
  
  # train VAE on corrupted data:
  vae = vae.train(XData=Xdata_Missing,training_epochs=n_epochs,train_obs=train_obs)
  
  # plot training history:
  # fig = plt.figure(dpi = 150)
  # plt.plot(vae.losshistory_epoch,vae.losshistory)
  # plt.xlabel('Epoch')
  # plt.ylabel('Evidence Lower Bound (ELBO)')
  # plt.show()
  '''
  ==============================================================================
  '''
  # IMPUTE MISSING VALUES
  # impute missing values:
  X_impute = vae.impute(X_corrupt = Xdata_Missing, max_iter = n_impute)
  
  # plot imputation results for sample values:
  # fig = plt.figure(dpi = 150)
  # subplotmax = min(NanCount,4)
  # for plotnum in range(subplotmax):
  #     TrueVal = Xdata[NanIndex[0][plotnum]][NanIndex[1][plotnum]]
  #     plt.subplot(subplotmax,1,plotnum+1)
  #     plt.plot(range(n_impute),vae.MissVal[:,plotnum],'-.')
  #     plt.plot([0, n_impute-1],[TrueVal, TrueVal])
  #     plt.xlabel('Iteration')
  #     plt.ylabel('Missing value ' + str(plotnum+1), fontsize=6)
  #     plt.tick_params(labelsize='small')
  # plt.show()
  
  # plot imputation results for one variable:
  var_i = 0
  min_i = np.min(Xdata[:,var_i])
  max_i = np.max(Xdata[:,var_i])
  
  # fig = plt.figure(dpi = 150)
  # plt.plot(Xdata[NanIndex[0][np.where(NanIndex[1]==var_i)],var_i],X_impute[NanIndex[0][np.where(NanIndex[1]==var_i)],var_i],'.')
  # plt.plot([min_i, max_i], [min_i, max_i])
  # plt.xlabel('True value')
  # plt.ylabel('Imputed value')
  # plt.show()
  
  # Standardise Xdata_Missing and Xdata wrt Xdata:
  Xdata = sc.inverse_transform(Xdata)
  X_impute = sc.inverse_transform(X_impute)
  
  Xdata = scRecon.transform(Xdata)
  X_impute = scRecon.transform(X_impute)
  
  ReconstructionError = sum(((X_impute[NanIndex] - Xdata[NanIndex])**2)**0.5)/NanCount
  print('Reconstruction error (VAE):')
  print(ReconstructionError)
  ReconstructionError_baseline = sum(((Xdata[NanIndex])**2)**0.5)/NanCount
  print('Reconstruction error (replace with mean):')
  print(ReconstructionError_baseline)
  '''
  ==============================================================================
  '''
  # GENERATE VALUES AND PLOT HISTOGRAMS
  np_x = next_batch(Xdata_Missing, n_batch_test)
  # reconstruct data by sampling from distribution of reconstructed variables:
  x_hat = vae.reconstruct(np_x, sample = 'sample')
  
  x_hat_prior = vae.generate(n_samples = 1000)
  x_hat_prior = x_hat_prior.eval()
  
  # subplotmax = min(n_x,5)
  # f, axarr = plt.subplots(subplotmax, subplotmax, sharex='col', dpi = 150)
  # f.suptitle('Posterior sample')
  # f.subplots_adjust(wspace = 0.3)
  # for k in range(subplotmax):
  #     for j in range(subplotmax):
  #         if k == j:
  #             axarr[k, j].hist(np_x[:,k],bins = 30, density=True)
  #             axarr[k, j].hist(x_hat[:,k],bins = 30, alpha = 0.7, density=True)
  #             axarr[k, j].tick_params(labelsize='xx-small', pad = 0)
  #             if j == 0:
  #                 axarr[k, j].set_ylabel('Variable 1', fontsize=6)
  #             elif j == subplotmax-1:
  #                 axarr[k, j].set_xlabel('Variable ' + str(subplotmax), fontsize=6)
  #         else:
  #             axarr[k, j].plot(np_x[:,k], np_x[:,j], '+',label = 'Data')
  #             axarr[k, j].plot(x_hat[:,k], x_hat[:,j], '.', alpha = 0.2, label='Posterior')
  #             axarr[k, j].tick_params(labelsize='xx-small', pad = 0)
  #             if j == 0:
  #                 axarr[k, j].set_ylabel('Variable ' + str(k+1), fontsize=6)
  #             if k == subplotmax-1:
  #                 axarr[k, j].set_xlabel('Variable ' + str(j+1), fontsize=6)
  # 
  # f, axarr = plt.subplots(subplotmax, subplotmax, sharex='col', dpi = 150)
  # f.suptitle('Prior sample')
  # f.subplots_adjust(wspace = 0.3)
  # for k in range(subplotmax):
  #     for j in range(subplotmax):
  #         if k == j:
  #             axarr[k, j].hist(np_x[:,k],bins = 30, density=True)
  #             axarr[k, j].hist(x_hat_prior[:,k],bins = 30, alpha = 0.7, density=True)
  #             axarr[k, j].tick_params(labelsize='xx-small', pad = 0)
  #             if j == 0:
  #                 axarr[k, j].set_ylabel('Variable 1', fontsize=6)
  #             elif j == subplotmax-1:
  #                 axarr[k, j].set_xlabel('Variable ' + str(subplotmax), fontsize=6)
  #         else:
  #             axarr[k, j].plot(np_x[:,k], np_x[:,j], '+',label = 'Data')
  #             axarr[k, j].plot(x_hat_prior[:,k], x_hat_prior[:,j], '.', alpha = 0.2, label='Prior')
  #             axarr[k, j].tick_params(labelsize='xx-small', pad = 0)
  #             if j == 0:
  #                 axarr[k, j].set_ylabel('Variable ' + str(k+1), fontsize=6)
  #             if k == subplotmax-1:
  #                 axarr[k, j].set_xlabel('Variable ' + str(j+1), fontsize=6)
  
  vae.sess.close()
  
  return {'Xdata': Xdata, 'X_impute': X_impute, 'np_x': np_x, 'x_hat': x_hat, 'Recon_error': ReconstructionError, 'Recon_error_baseline': ReconstructionError_baseline, 'ELBO_train': vae.losshistory}

# np_x: batch of 2000 from Xdata_Missing. x_hat: reconstruction of np_x
# ReconstructionError: essentially MSE of masked values


#################################################################################################
## Class VAE (Kingma 2014), doesn't handle missing data
def run_VAE(Data,Missing):
  return(Data)
  
#################################################################################################

## not yet working. Smieja et al (2019): missingness through a dropout layer/computes some kind of expectation
def run_Smieja_ae(data,Missing,lr=0.01,n_epochs=250,bs=64,num_hidden_1=256,num_hidden_2=128,num_hidden_3=64):
  
  num_input = 784  # MNIST data_rbfn input (img shape: 28*28)
  n_distribution = 5  # number of n_distribution
  width_mask = 13  # size of window mask
  
  from datetime import datetime
  from time import time
  import numpy as np
  import tensorflow as tf
  from sklearn.impute import SimpleImputer
  from sklearn.mixture import GaussianMixture
  from tqdm import tqdm
  
  #%% Train Test Division (from GAIN)
  idx = np.random.permutation(No)
  
  Train_No = int(No * train_rate)
  Test_No = No - Train_No
      
  # Train / Test Features
  trainX = Data[idx[:Train_No],:]
  testX = Data[idx[Train_No:],:]
  
  # Train / Test Missing Indicators
  trainM = Missing[idx[:Train_No],:]
  testM = Missing[idx[Train_No:],:]
  import os
  
  os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
  
  RANDOM_SEED = 42
  tf.set_random_seed(RANDOM_SEED)
  
  # tf Graph input (only pictures)
  X = tf.placeholder("float", [None, num_input])
  
  initializer = tf.contrib.layers.variance_scaling_initializer()
  
  weights = {
      'encoder_h1': tf.Variable(initializer([num_input, num_hidden_1])),
      'encoder_h2': tf.Variable(initializer([num_hidden_1, num_hidden_2])),
      'encoder_h3': tf.Variable(initializer([num_hidden_2, num_hidden_3])),
      'decoder_h1': tf.Variable(initializer([num_hidden_3, num_hidden_2])),
      'decoder_h2': tf.Variable(initializer([num_hidden_2, num_hidden_1])),
      'decoder_h3': tf.Variable(initializer([num_hidden_1, num_input])),
  }
  biases = {
      'encoder_b1': tf.Variable(tf.random_normal([num_hidden_1])),
      'encoder_b2': tf.Variable(tf.random_normal([num_hidden_2])),
      'encoder_b3': tf.Variable(tf.random_normal([num_hidden_3])),
      'decoder_b1': tf.Variable(tf.random_normal([num_hidden_2])),
      'decoder_b2': tf.Variable(tf.random_normal([num_hidden_1])),
      'decoder_b3': tf.Variable(tf.random_normal([num_input])),
  }
  
  
  def random_mask(width_window, margin=0):
      margin_left = margin
      margin_righ = margin
      margin_top = margin
      margin_bottom = margin
      start_width = margin_top + np.random.randint(28 - width_window - margin_top - margin_bottom)
      start_height = margin_left + np.random.randint(28 - width_window - margin_left - margin_righ)
  
      return np.concatenate([28 * i + np.arange(start_height, start_height + width_window) for i in
                             np.arange(start_width, start_width + width_window)], axis=0).astype(np.int32)
  
  
  def data_with_mask(x, width_window=10):
      h = width_window
      for i in range(x.shape[0]):
          if width_window <= 0:
              h = np.random.randint(8, 20)
          maska = random_mask(h)
          x[i, maska] = np.nan
      return x
  
  
  def nr(mu, sigma):
      non_zero = tf.not_equal(sigma, 0.)
      new_sigma = tf.where(non_zero, sigma, tf.fill(tf.shape(sigma), 1e-20))
      sqrt_sigma = tf.sqrt(new_sigma)
  
      w = tf.div(mu, sqrt_sigma)
      nr_values = sqrt_sigma * (tf.div(tf.exp(tf.div(-tf.square(w), 2.)), np.sqrt(2 * np.pi)) +
                                tf.multiply(tf.div(w, 2.), 1 + tf.erf(tf.div(w, np.sqrt(2)))))
  
      nr_values = tf.where(non_zero, nr_values, (mu + tf.abs(mu)) / 2.)
      return nr_values
  
  
  def conv_first(x, means, covs, p, gamma):
      gamma_ = tf.abs(gamma)
      # gamma_ = tf.cond(tf.less(gamma_[0], 1.), lambda: gamma_, lambda: tf.square(gamma_))
      covs_ = tf.abs(covs)
      p_ = tf.nn.softmax(p, axis=0)
  
      check_isnan = tf.is_nan(x)
      check_isnan = tf.reduce_sum(tf.cast(check_isnan, tf.int32), 1)
  
      x_miss = tf.gather(x, tf.reshape(tf.where(check_isnan > 0), [-1]))  # data_rbfn with missing values
      x = tf.gather(x, tf.reshape(tf.where(tf.equal(check_isnan, 0)), [-1]))  # data_rbfn without missing values
  
      # data_rbfn without missing
      layer_1 = tf.nn.relu(tf.add(tf.matmul(x, weights['encoder_h1']), biases['encoder_b1']))
  
      # data_rbfn with missing
      where_isnan = tf.is_nan(x_miss)
      where_isfinite = tf.is_finite(x_miss)
      size = tf.shape(x_miss)
  
      weights2 = tf.square(weights['encoder_h1'])
  
      # Collect distributions
      distributions = tf.TensorArray(dtype=x.dtype, size=n_distribution)
      q_collector = tf.TensorArray(dtype=x.dtype, size=n_distribution)
  
      # Each loop iteration calculates all per component
      def calculate_component(i, collect1, collect2):
          data_miss = tf.where(where_isnan, tf.reshape(tf.tile(means[i, :], [size[0]]), [-1, size[1]]), x_miss)
          miss_cov = tf.where(where_isnan, tf.reshape(tf.tile(covs_[i, :], [size[0]]), [-1, size[1]]),
                              tf.zeros([size[0], size[1]]))
  
          layer_1_m = tf.add(tf.matmul(data_miss, weights['encoder_h1']), biases['encoder_b1'])
          layer_1_m = nr(layer_1_m, tf.matmul(miss_cov, weights2))
  
          norm = tf.subtract(data_miss, means[i, :])
          norm = tf.square(norm)
          q = tf.where(where_isfinite,
                       tf.reshape(tf.tile(tf.add(gamma_, covs_[i, :]), [size[0]]), [-1, size[1]]),
                       tf.ones_like(x_miss))
          norm = tf.div(norm, q)
          norm = tf.reduce_sum(norm, axis=1)
  
          q = tf.log(q)
          q = tf.reduce_sum(q, axis=1)
  
          q = tf.add(q, norm)
  
          norm = tf.cast(tf.reduce_sum(tf.cast(where_isfinite, tf.int32), axis=1), tf.float32)
          norm = tf.multiply(norm, tf.log(2 * np.pi))
  
          q = tf.add(q, norm)
          q = -0.5 * q
  
          return i + 1, collect1.write(i, layer_1_m), collect2.write(i, q)
  
      i = tf.constant(0)
      _, final_distributions, final_q = tf.while_loop(lambda i, c1, c2: i < n_distribution, calculate_component,
                                                      loop_vars=(i, distributions, q_collector),
                                                      swap_memory=True, parallel_iterations=1)
  
      distrib = final_distributions.stack()
      log_q = final_q.stack()
  
      log_q = tf.add(log_q, tf.log(p_))
      r = tf.nn.softmax(log_q, axis=0)
  
      layer_1_miss = tf.multiply(distrib, r[:, :, tf.newaxis])
      layer_1_miss = tf.reduce_sum(layer_1_miss, axis=0)
  
      # join layer for data_rbfn with missing values with layer for data_rbfn without missing values
      layer_1 = tf.concat((layer_1, layer_1_miss), axis=0)
      return layer_1
  
  
  # Building the encoder
  def encoder(x, means, covs, p, gamma):
      layer_1 = conv_first(x, means, covs, p, gamma)
  
      # Encoder Hidden layer with sigmoid activation
      layer_2 = tf.nn.sigmoid(tf.add(tf.matmul(layer_1, weights['encoder_h2']), biases['encoder_b2']))
      layer_3 = tf.nn.sigmoid(tf.add(tf.matmul(layer_2, weights['encoder_h3']), biases['encoder_b3']))
      return layer_3
  
  
  # Building the decoder
  def decoder(x):
      layer_1 = tf.nn.sigmoid(tf.add(tf.matmul(x, weights['decoder_h1']), biases['decoder_b1']))
      layer_2 = tf.nn.sigmoid(tf.add(tf.matmul(layer_1, weights['decoder_h2']), biases['decoder_b2']))
      layer_3 = tf.nn.sigmoid(tf.add(tf.matmul(layer_2, weights['decoder_h3']), biases['decoder_b3']))
      return layer_3
  
  
  def prep_x(x):
      check_isnan = tf.is_nan(x)
      check_isnan = tf.reduce_sum(tf.cast(check_isnan, tf.int32), 1)
  
      x_miss = tf.gather(x, tf.reshape(tf.where(check_isnan > 0), [-1]))
      x = tf.gather(x, tf.reshape(tf.where(tf.equal(check_isnan, 0)), [-1]))
      return tf.concat((x, x_miss), axis=0)
  
  
  t0 = time()
  mnist = tf.keras.datasets.mnist
  try:
      with np.load('./data/mnist.npz') as f:
          x_train, y_train = f['x_train'], f['y_train']
          x_test, y_test = f['x_test'], f['y_test']
  except FileNotFoundError:
      (x_train, y_train), (x_test, y_test) = mnist.load_data()   # 60K images of digits, 28 by 28 pixels each
  x_train, x_test = x_train / 255.0, x_test / 255.0     # scale to 0 to 1
  print("Read data done in %0.3fs." % (time() - t0))
  
  data_train = x_train
  
  # choose test images nn * 10
  nn = 100
  data_test = x_test[np.where(y_test == 0)[0][:nn], :]
  for i in range(1, 10):
      data_test = np.concatenate([data_test, x_test[np.where(y_test == i)[0][:nn], :]], axis=0)
  data_test = np.random.permutation(data_test)
  
  del mnist
  
  # change background to white
  data_train = 1. - data_train.reshape(-1, num_input)
  data_test = 1. - data_test.reshape(-1, num_input)
  
  # create missing data_rbfn train
  data_train = data_with_mask(data_train, width_mask)
  
  # create missing data_rbfn test
  data_test = data_with_mask(data_test, width_mask)
  
  imp = SimpleImputer(missing_values=np.nan, strategy='mean')
  data = imp.fit_transform(data_train)
  
  t0 = time()
  gmm = GaussianMixture(n_components=n_distribution, covariance_type='diag').fit(data)
  print("GMM done in %0.3fs." % (time() - t0))
  
  p = tf.Variable(initial_value=np.log(gmm.weights_.reshape((-1, 1))), dtype=tf.float32)
  means = tf.Variable(initial_value=gmm.means_, dtype=tf.float32)
  covs = tf.Variable(initial_value=gmm.covariances_, dtype=tf.float32)
  gamma = tf.Variable(initial_value=tf.random_normal(shape=(1,), mean=1., stddev=1.), dtype=tf.float32)
  del data, gmm
  
  # Construct model
  encoder_op = encoder(X, means, covs, p, gamma)
  decoder_op = decoder(encoder_op)
  
  y_pred = decoder_op  # prediction
  y_true = prep_x(X)  # Targets (Labels) are the input data_rbfn.
  
  where_isnan = tf.is_nan(y_true)
  y_pred = tf.where(where_isnan, tf.zeros_like(y_pred), y_pred)
  y_true = tf.where(where_isnan, tf.zeros_like(y_true), y_true)
  
  # Define loss and optimizer, minimize the squared error
  loss = tf.reduce_mean(tf.pow(y_true - y_pred, 2))
  optimizer = tf.train.RMSPropOptimizer(lr).minimize(loss)
  
  # Initialize the variables (i.e. assign their default value)
  init = tf.global_variables_initializer()
  
  trn_summary = [[] for _ in range(5)]
  trn_imgs = [[] for _ in range(2)]
  with tf.name_scope('train'):
      trn_summary[0] = tf.summary.scalar('loss', loss)
      trn_summary[1] = tf.summary.histogram("p", tf.nn.softmax(p, axis=0))
      for i in range(n_distribution):
          trn_summary[2].append(tf.summary.histogram("mean/{:d}".format(i), means[i]))
          trn_summary[3].append(tf.summary.histogram("cov/{:d}".format(i), tf.abs(covs[i])))
      trn_summary[4] = tf.summary.scalar('gamma', tf.abs(gamma)[0])
      image_grid = tf.contrib.gan.eval.image_grid(tf.gather(prep_x(X), np.arange(25)), (5, 5), (28, 28), 1)
      trn_imgs[0] = tf.summary.image('input', image_grid, 1)
      image_grid = tf.contrib.gan.eval.image_grid(tf.gather(decoder_op, np.arange(25)), (5, 5), (28, 28), 1)
      trn_imgs[1] = tf.summary.image('output', image_grid, 1)
  
  tst_summary = [[] for _ in range(3)]
  with tf.name_scope('test'):
      tst_summary[0] = tf.summary.scalar('loss', loss)
      image_grid = tf.contrib.gan.eval.image_grid(tf.gather(prep_x(X), np.arange(25)), (5, 5), (28, 28), 1)
      tst_summary[1] = tf.summary.image('input', image_grid, 1)
      image_grid = tf.contrib.gan.eval.image_grid(tf.gather(decoder_op, np.arange(25)), (5, 5), (28, 28), 1)
      tst_summary[2] = tf.summary.image('output', image_grid, 1)
  
  current_date = datetime.now()
  current_date = current_date.strftime('%d%b_%H%M%S')
  
  with tf.Session() as sess:
      train_writer = tf.summary.FileWriter('./log/{}'.format(current_date), sess.graph)
      sess.run(init)  # run the initializer
  
      res = sess.run([*trn_summary], feed_dict={X: data_test[:25]})
  
      train_writer.add_summary(res[1], -1)
      for i in range(n_distribution):
          train_writer.add_summary(res[2][i], -1)
          train_writer.add_summary(res[3][i], -1)
      train_writer.add_summary(res[4], -1)
  
      epoch_tqdm = tqdm(range(1, n_epochs + 1), desc="Loss", leave=False)
      for epoch in epoch_tqdm:
          n_batch = data_train.shape[0] // bs
          for iteration in tqdm(range(n_batch), desc="Batches", leave=False):
              batch_x = data_train[(iteration * bs):((iteration + 1) * bs), :]
  
              # Run optimization op (backprop) and cost op (to get loss value)
              res = sess.run([optimizer, loss, *trn_summary, *trn_imgs], feed_dict={X: batch_x})
  
              train_writer.add_summary(res[-2], n_batch * (epoch - 1) + iteration)
              train_writer.add_summary(res[-1], n_batch * (epoch - 1) + iteration)
              train_writer.add_summary(res[2], n_batch * (epoch - 1) + iteration)
              train_writer.add_summary(res[3], n_batch * (epoch - 1) + iteration)
              for i in range(n_distribution):
                  train_writer.add_summary(res[4][i], n_batch * (epoch - 1) + iteration)
                  train_writer.add_summary(res[5][i], n_batch * (epoch - 1) + iteration)
              train_writer.add_summary(res[6], n_batch * (epoch - 1) + iteration)
  
              epoch_tqdm.set_description("Loss: {:.5f}".format(res[1]))
  
          tst_loss, tst_input, tst_output = sess.run([*tst_summary], feed_dict={X: data_test[:25]})
          train_writer.add_summary(tst_loss, epoch)
          train_writer.add_summary(tst_input, epoch)
          train_writer.add_summary(tst_output, epoch)




















## MIWAE (Mattei et al 2019):
# h: num_neurons/layers, sigma: activation function, bs: batch size, n_epochs: num_epochs, lr: learning rate
# d: dimensions of latent space, niw: number of importance weights in training (built in. can be changed, but different for training vs imputation)
# def run_MIWAE(data,Missing,norm_means,norm_sds,h=128,sigma="relu",bs = 64,n_epochs = 501,lr=0.001,niw=20,dim_z=10):
#   import tensorflow as tf
#   import numpy as np
#   import scipy.stats
#   import scipy.io
#   import scipy.sparse
#   from scipy.io import loadmat
#   import pandas as pd
#   import tensorflow_probability as tfp
#   tfd = tfp.distributions
#   tfk = tf.keras
#   tfkl = tf.keras.layers
#   # from PIL import Image
#   import matplotlib.pyplot as plt
#   from sklearn.metrics import f1_score
#   from sklearn import preprocessing
#   import pandas as pd
#   from sklearn.ensemble import ExtraTreesRegressor
#   from sklearn.experimental import enable_iterative_imputer 
#   from sklearn.linear_model import BayesianRidge
#   from sklearn.impute import IterativeImputer
#   from sklearn.impute import SimpleImputer
#   def mse(xhat,xtrue,mask): # MSE function for imputations
#       xhat = np.array(xhat)
#       xtrue = np.array(xtrue)
#       return np.mean(np.power(xhat-xtrue,2)[~mask])
#   import time
#   
#   time0 = time.time()
#   
#   xfull = (data - np.mean(data,0))/np.std(data,0)
#   n = xfull.shape[0] # number of observations
#   p = xfull.shape[1] # number of features
#   np.random.seed(1234)
#   tf.set_random_seed(1234)
#   
#   xmiss = np.copy(xfull)
#   xmiss[Missing==0]=np.nan
#   mask = np.isfinite(xmiss) # binary mask that indicates which values are missing
#   
#   xhat_0 = np.copy(xmiss)
#   xhat_0[np.isnan(xmiss)] = 0
#   
#   ##Placeholders and hyperparameters
#   x = tf.placeholder(tf.float32, shape=[None, p]) # Placeholder for xhat_0
#   learning_rate = tf.placeholder(tf.float32, shape=[])
#   batch_size = tf.shape(x)[0]
#   xmask = tf.placeholder(tf.bool, shape=[None, p])
#   K= tf.placeholder(tf.int32, shape=[]) # Placeholder for the number of importance weights
#   
#   ##Model Building
#   # d = np.floor(p/2).astype(int) # dimension of the latent space
#   d=dim_z
#   p_z = tfd.MultivariateNormalDiag(loc=tf.zeros(d, tf.float32))
#   
#   # building decoder
#   decoder = tfk.Sequential([
#     tfkl.InputLayer(input_shape=[d,]),
#     tfkl.Dense(h, activation=sigma,kernel_initializer="orthogonal"),
#     tfkl.Dense(h, activation=sigma,kernel_initializer="orthogonal"),
#     tfkl.Dense(3*p,kernel_initializer="orthogonal") # the decoder will output both the mean, the scale, and the number of degrees of freedoms (hence the 3*p)
#   ])
#   
#   # building encoder
#   tiledmask = tf.tile(xmask,[K,1])
#   tiledmask_float = tf.cast(tiledmask,tf.float32)
#   mask_not_float = tf.abs(-tf.cast(xmask,tf.float32))
#   
#   iota = tf.Variable(np.zeros([1,p]),dtype=tf.float32)
#   tilediota = tf.tile(iota,[batch_size,1])
#   iotax = x + tf.multiply(tilediota,mask_not_float)
#   
#   encoder = tfk.Sequential([
#     tfkl.InputLayer(input_shape=[p,]),
#     tfkl.Dense(h, activation=sigma,kernel_initializer="orthogonal"),
#     tfkl.Dense(h, activation=sigma,kernel_initializer="orthogonal"),
#     tfkl.Dense(3*d,kernel_initializer="orthogonal")
#   ])
#   
#   ##Building MIWAE bound
#   
#   out_encoder = encoder(iotax)
#   q_zgivenxobs = tfd.Independent(distribution=tfd.StudentT(loc=out_encoder[..., :d], scale=tf.nn.softplus(out_encoder[..., d:(2*d)]), df=3 + tf.nn.softplus(out_encoder[..., (2*d):(3*d)])))
#   zgivenx = q_zgivenxobs.sample(K)
#   zgivenx_flat = tf.reshape(zgivenx,[K*batch_size,d])
#   data_flat = tf.reshape(tf.tile(x,[K,1]),[-1,1])
#   out_decoder = decoder(zgivenx_flat)
#   all_means_obs_model = out_decoder[..., :p]
#   all_scales_obs_model = tf.nn.softplus(out_decoder[..., p:(2*p)]) + 0.001
#   all_degfreedom_obs_model = tf.nn.softplus(out_decoder[..., (2*p):(3*p)]) + 3
#   all_log_pxgivenz_flat = tfd.StudentT(loc=tf.reshape(all_means_obs_model,[-1,1]),scale=tf.reshape(all_scales_obs_model,[-1,1]),df=tf.reshape(all_degfreedom_obs_model,[-1,1])).log_prob(data_flat)
#   all_log_pxgivenz = tf.reshape(all_log_pxgivenz_flat,[K*batch_size,p])
#   logpxobsgivenz = tf.reshape(tf.reduce_sum(tf.multiply(all_log_pxgivenz,tiledmask_float),1),[K,batch_size])
#   logpz = p_z.log_prob(zgivenx)
#   logq = q_zgivenxobs.log_prob(zgivenx)
#   
#   miwae_loss = -tf.reduce_mean(tf.reduce_logsumexp(logpxobsgivenz + logpz - logq,0)) +tf.log(tf.cast(K,tf.float32))
#   train_miss = tf.train.AdamOptimizer(learning_rate = learning_rate).minimize(miwae_loss)
#   
#   
#   ##Single imputation
#   xgivenz = tfd.Independent(
#         distribution=tfd.StudentT(loc=all_means_obs_model, scale=all_scales_obs_model, df=all_degfreedom_obs_model))
#   imp_weights = tf.nn.softmax(logpxobsgivenz + logpz - logq,0) # these are w_1,....,w_L for all observations in the batch
#   xms = tf.reshape(xgivenz.mean(),[K,batch_size,p])
#   xm=tf.einsum('ki,kij->ij', imp_weights, xms)
#   
#   ##Training and imputing
#   miwae_loss_train=np.array([])
#   mse_train=np.array([])
#   
#   time_train=np.array([])
#   time_impute=np.array([])
#   xhat = np.copy(xhat_0) # This will be out imputed data matrix
#   
#   MIWAE_LB_epoch=np.array([])
#   with tf.Session() as sess:
#       sess.run(tf.global_variables_initializer())
#       for ep in range(1,n_epochs):
#         perm = np.random.permutation(n) # We use the "random reshuffling" version of SGD
#         batches_data = np.array_split(xhat_0[perm,], n/bs)
#         batches_mask = np.array_split(mask[perm,], n/bs)
#         t0_train=time.time()
#         for it in range(len(batches_data)):
#             train_miss.run(feed_dict={x: batches_data[it], learning_rate: lr, K: niw, xmask: batches_mask[it]}) # Gradient step      
#         time_train=np.append(time_train,time.time()-t0_train)
#         losstrain = np.array([miwae_loss.eval(feed_dict={x: xhat_0, K: niw, xmask: mask})]) # MIWAE bound evaluation
#         miwae_loss_train = np.append(miwae_loss_train,-losstrain,axis=0)
#         MIWAE_LB_epoch=np.append(MIWAE_LB_epoch,-losstrain)
#         if ep % 100 == 1:
#             # custom to save latent dimensions
#             latents = out_encoder.eval(feed_dict={x: xhat_0, xmask:mask})
#             mus=latents[:,0:d]
#             sigmas=latents[:,d:2*d]
#             dfs=latents[:,2*d:3*d]
#             Zs = zgivenx.eval(feed_dict={x: xhat_0,K: niw, xmask:mask})
#             
#             print('Epoch %g' %ep)
#             print('MIWAE likelihood bound  %g' %-losstrain)
#             
#             t0_impute=time.time()
#             for i in range(n): # We impute the observations one at a time for memory reasons
#                 xhat[i,:][~mask[i,:]]=xm.eval(feed_dict={x: xhat_0[i,:].reshape([1,p]), K:1000, xmask: mask[i,:].reshape([1,p])})[~mask[i,:].reshape([1,p])]
#             time_impute=np.append(time_impute,time.time()-t0_impute)
#             
#             err = np.array([mse(xhat,xfull,mask)])
#             mse_train = np.append(mse_train,err,axis=0)
#             print('Imputation MSE  %g' %err)
#             print('-----')
#   
#   print("miwae done. Running mf and mean")
#   time1 = time.time()
# 
#   time_miwae = time1-time0
#   
#   return {'MIWAE_LB_epoch': MIWAE_LB_epoch, 'time_train': time_train,'time_impute': time_impute,'time_miwae': time_miwae,'imp_weights': imp_weights,'MSE': mse_train, 'xhat': xhat, 'mask': mask, 'latents': latents, 'mus': mus, 'sigmas': sigmas, 'dfs': dfs, 'Zs': Zs}
#   
# def run_MIWAE_GPU(data,Missing,train=1,norm_means,norm_sds,saved_model=None,h=128,sigma="relu",bs = 64,n_epochs = 501,lr=0.001,niw=20,dim_z=10,L=100):
#   # L: number of MC samples used in imputation
#   import torch
#   import torchvision
#   import torch.nn as nn
#   import numpy as np
#   import scipy.stats
#   import scipy.io
#   import scipy.sparse
#   from scipy.io import loadmat
#   import pandas as pd
#   import matplotlib.pyplot as plt
#   import torch.distributions as td
# 
#   from torch import nn, optim
#   from torch.nn import functional as F
#   from torchvision import datasets, transforms
#   from torchvision.utils import save_image
# 
#   import time
#   
#   def mse(xhat,xtrue,mask): # MSE function for imputations
#     xhat = np.array(xhat)
#     xtrue = np.array(xtrue)
#     return np.mean(np.power(xhat-xtrue,2)[~mask])
#   
#   time0 = time.time()
#     
#   # xfull = (data - np.mean(data,0))/np.std(data,0)
#   xfull = (data - norm_means)/norm_sds
#   n = xfull.shape[0] # number of observations
#   p = xfull.shape[1] # number of features
#   
#   np.random.seed(1234)
#   
#   xmiss = np.copy(xfull)
#   xmiss[Missing==0]=np.nan
#   mask = np.isfinite(xmiss) # binary mask that indicates which values are missing
#   
#   xhat_0 = np.copy(xmiss)
#   xhat_0[np.isnan(xmiss)] = 0
#   
#   d = dim_z # dimension of the latent space
#   K = niw # number of IS during training
# 
#   p_z = td.Independent(td.Normal(loc=torch.zeros(d).cuda(),scale=torch.ones(d).cuda()),1)     # THIS IS NORMAL vs. student T used in CPU version!!
#   
#   decoder = nn.Sequential(
#     torch.nn.Linear(d, h),
#     torch.nn.ReLU(),
#     torch.nn.Linear(h, h),
#     torch.nn.ReLU(),
#     torch.nn.Linear(h, 3*p),  # the decoder will output both the mean, the scale, and the number of degrees of freedoms (hence the 3*p)
#   )
#   encoder = nn.Sequential(
#     torch.nn.Linear(p, h),
#     torch.nn.ReLU(),
#     torch.nn.Linear(h, h),
#     torch.nn.ReLU(),
#     torch.nn.Linear(h, 2*d),  # the encoder will output both the mean and the diagonal covariance
#   )
#   encoder.cuda() # we'll use the GPU
#   decoder.cuda()
#   
#   def miwae_loss(iota_x,mask):
#     batch_size = iota_x.shape[0]
#     out_encoder = encoder(iota_x)
#     q_zgivenxobs = td.Independent(td.Normal(loc=out_encoder[..., :d],scale=torch.nn.Softplus()(out_encoder[..., d:(2*d)])),1)
#     
#     zgivenx = q_zgivenxobs.rsample([K])
#     zgivenx_flat = zgivenx.reshape([K*batch_size,d])
#     
#     out_decoder = decoder(zgivenx_flat)
#     all_means_obs_model = out_decoder[..., :p]
#     all_scales_obs_model = torch.nn.Softplus()(out_decoder[..., p:(2*p)]) + 0.001
#     all_degfreedom_obs_model = torch.nn.Softplus()(out_decoder[..., (2*p):(3*p)]) + 3
#     
#     data_flat = torch.Tensor.repeat(iota_x,[K,1]).reshape([-1,1])
#     tiledmask = torch.Tensor.repeat(mask,[K,1])
#     
#     all_log_pxgivenz_flat = torch.distributions.StudentT(loc=all_means_obs_model.reshape([-1,1]),scale=all_scales_obs_model.reshape([-1,1]),df=all_degfreedom_obs_model.reshape([-1,1])).log_prob(data_flat)
#     all_log_pxgivenz = all_log_pxgivenz_flat.reshape([K*batch_size,p])     # p(x|z) : Product of 1-D student's T. q(z|x) : MV-Gaussian
#     
#     logpxobsgivenz = torch.sum(all_log_pxgivenz*tiledmask,1).reshape([K,batch_size])
#     logpz = p_z.log_prob(zgivenx)
#     logq = q_zgivenxobs.log_prob(zgivenx)
#     
#     neg_bound = -torch.mean(torch.logsumexp(logpxobsgivenz + logpz - logq,0))
#     return neg_bound
#   
#   optimizer = optim.Adam(list(encoder.parameters()) + list(decoder.parameters()),lr=lr)
#   
#   def miwae_impute(iota_x,mask,L):
#     batch_size = iota_x.shape[0]
#     out_encoder = encoder(iota_x)
# 
#     q_zgivenxobs = td.Independent(td.Normal(loc=out_encoder[..., :d],scale=torch.nn.Softplus()(out_encoder[..., d:(2*d)])),1)
#     
#     zgivenx = q_zgivenxobs.rsample([L])
#     zgivenx_flat = zgivenx.reshape([L*batch_size,d])
#     
#     out_decoder = decoder(zgivenx_flat)
#     all_means_obs_model = out_decoder[..., :p]
#     all_scales_obs_model = torch.nn.Softplus()(out_decoder[..., p:(2*p)]) + 0.001
#     all_degfreedom_obs_model = torch.nn.Softplus()(out_decoder[..., (2*p):(3*p)]) + 3
#     
#     data_flat = torch.Tensor.repeat(iota_x,[L,1]).reshape([-1,1]).cuda()
#     tiledmask = torch.Tensor.repeat(mask,[L,1]).cuda()
#     
#     all_log_pxgivenz_flat = torch.distributions.StudentT(loc=all_means_obs_model.reshape([-1,1]),scale=all_scales_obs_model.reshape([-1,1]),df=all_degfreedom_obs_model.reshape([-1,1])).log_prob(data_flat)
#     all_log_pxgivenz = all_log_pxgivenz_flat.reshape([L*batch_size,p])
#     
#     logpxobsgivenz = torch.sum(all_log_pxgivenz*tiledmask,1).reshape([L,batch_size])
#     logpz = p_z.log_prob(zgivenx)
#     logq = q_zgivenxobs.log_prob(zgivenx)
#     
#     xgivenz = td.Independent(td.StudentT(loc=all_means_obs_model, scale=all_scales_obs_model, df=all_degfreedom_obs_model),1)
#     
#     imp_weights = torch.nn.functional.softmax(logpxobsgivenz + logpz - logq,0) # these are w_1,....,w_L for all observations in the batch
#     xms = xgivenz.sample().reshape([L,batch_size,p])
#     xm=torch.einsum('ki,kij->ij', imp_weights, xms) 
#     return {'xm': xm, 'out_encoder': out_encoder,'imp_weights': imp_weights,'zgivenx_flat': zgivenx_flat}
#   def weights_init(layer):
#     if type(layer) == nn.Linear: torch.nn.init.orthogonal_(layer.weight)
#   
#   miwae_loss_train=np.array([])
#   mse_train=np.array([])
#   mse_test=np.array([])
#   mse_train2=np.array([])
#   bs = bs # batch size
#   n_epochs = n_epochs
#   xhat = np.copy(xhat_0) # This will be out imputed data matrix
#   
#   encoder.apply(weights_init)
#   decoder.apply(weights_init)
#   
#   time_train=[]
#   time_impute=[]
#   MIWAE_LB_epoch=[]
#   if train==1:
#     # Training+Imputing
#     for ep in range(1,n_epochs):
#       perm = np.random.permutation(n) # We use the "random reshuffling" version of SGD
#       batches_data = np.array_split(xhat_0[perm,], n/bs)
#       batches_mask = np.array_split(mask[perm,], n/bs)
#       t0_train=time.time()
#       for it in range(len(batches_data)):
#         optimizer.zero_grad()
#         encoder.zero_grad()
#         decoder.zero_grad()
#         b_data = torch.from_numpy(batches_data[it]).float().cuda()
#         b_mask = torch.from_numpy(batches_mask[it]).float().cuda()
#         loss = miwae_loss(iota_x = b_data,mask = b_mask)
#         loss.backward()
#         optimizer.step()
#       time_train=np.append(time_train,time.time()-t0_train)
#       MIWAE_LB=(-np.log(K)-miwae_loss(iota_x = torch.from_numpy(xhat_0).float().cuda(),mask = torch.from_numpy(mask).float().cuda()).cpu().data.numpy())
#       MIWAE_LB_epoch=np.append(MIWAE_LB_epoch,MIWAE_LB)
#       if ep % 100 == 1:
#         print('Epoch %g' %ep)
#         print('MIWAE likelihood bound  %g' %MIWAE_LB) # Gradient step      
#         ### Now we do the imputation
#         t0_impute=time.time()
#         xhat_fit=miwae_impute(iota_x = torch.from_numpy(xhat_0).float().cuda(),mask = torch.from_numpy(mask).float().cuda(),L=L)
#         time_impute=np.append(time_impute,time.time()-t0_impute)
#         xhat[~mask] = xhat_fit['xm'].cpu().data.numpy()[~mask]
#         # out_encoder = xhat_fit['out_encoder']
#         err = np.array([mse(xhat,xfull,mask)])
#         mse_train = np.append(mse_train,err,axis=0)
#         
#         # # custom to save latent dimensions GOTTA PUT THIS INTO THE DEFN FUNCTION
#         # latents = encoder.eval(feed_dict={x: xhat_0, xmask:mask})
#         # mus=latents[:,0:d]
#         # sigmas=latents[:,d:2*d]
#         # Zs = zgivenx.eval(feed_dict={x: xhat_0,K: niw, xmask:mask})
#         zgivenx_flat = xhat_fit['zgivenx_flat'].cpu().data.numpy()   # L samples*batch_size x d (d: latent dimension)
#         imp_weights=xhat_fit['imp_weights'].cpu().data.numpy()
#       
#         print('Imputation MSE  %g' %err)
#         print('-----')
#     saved_model={'encoder': encoder, 'decoder': decoder}
#     return {'saved_model': saved_model,'zgivenx_flat': zgivenx_flat,'MIWAE_LB_epoch': MIWAE_LB_epoch,'time_train': time_train,'time_impute': time_impute,'imp_weights': imp_weights,'MSE': mse_train, 'xhat': xhat, 'mask': mask}
#   else:
#     # validating (hyperparameter values) or testing
#     encoder=saved_model['encoder']
#     decoder=saved_model['decoder']
#     for ep in range(1,n_epochs):
#       #perm = np.random.permutation(n) # We use the "random reshuffling" version of SGD
#       #batches_data = np.array_split(xhat_0[perm,], n/bs)
#       #batches_mask = np.array_split(mask[perm,], n/bs)
#       #for it in range(len(batches_data)):
#       #  optimizer.zero_grad()
#       #  encoder.zero_grad()
#       #  decoder_x.zero_grad()
#       #  decoder_r.zero_grad()
#       #  b_data = torch.from_numpy(batches_data[it]).float().cuda()
#       #  b_mask = torch.from_numpy(batches_mask[it]).float().cuda()
#       #  loss = nimiwae_loss(iota_x = b_data,mask = b_mask)
#       #  loss.backward()
#       #  optimizer.step()
#       #time_train=np.append(time_train,time.time()-t0_train)
#       MIWAE_LB=(-np.log(K)-miwae_loss(iota_x = torch.from_numpy(xhat_0).float().cuda(),mask = torch.from_numpy(mask).float().cuda()).cpu().data.numpy())
#       print('Epoch %g' %ep)
#       print('MIWAE likelihood bound  %g' %MIWAE_LB) # Gradient step      
#       ### Now we do the imputation
#       t0_impute=time.time()
#       xhat_fit=miwae_impute(iota_x = torch.from_numpy(xhat_0).float().cuda(),mask = torch.from_numpy(mask).float().cuda(),L=L)
#       time_impute=np.append(time_impute,time.time()-t0_impute)
#       xhat[~mask] = xhat_fit['xm'].cpu().data.numpy()[~mask]
#       err = np.array([mse(xhat,xfull,mask)])
#       mse_test = np.append(mse_test,err,axis=0)
#       zgivenx_flat = xhat_fit['zgivenx_flat'].cpu().data.numpy()   # L samples*batch_size x d (d: latent dimension)
#       imp_weights = xhat_fit['imp_weights'].cpu().data.numpy()
#       print('Imputation MSE  %g' %err)
#       print('-----')
#     return {'zgivenx_flat': zgivenx_flat,'MIWAE_LB': MIWAE_LB,'time_impute': time_impute,'imp_weights': imp_weights,'MSE': mse_test, 'xhat': xhat, 'mask': mask}  
# 
#   
#   # t0_mf=time.time()
#   # missforest = IterativeImputer(max_iter=20, estimator=ExtraTreesRegressor(n_estimators=20))
#   # missforest.fit(xmiss)
#   # xhat_mf = missforest.transform(xmiss)
#   # time_mf=time.time()-t0_mf
#   # # iterative_ridge = IterativeImputer(max_iter=20, estimator=BayesianRidge())
#   # # iterative_ridge.fit(xmiss)
#   # # xhat_ridge = iterative_ridge.transform(xmiss)
#   # t0_mean=time.time()
#   # mean_imp = SimpleImputer(missing_values=np.nan, strategy='mean')
#   # mean_imp.fit(xmiss)
#   # xhat_mean = mean_imp.transform(xmiss)
#   # time_mean=time.time()-t0_mean
#   
#   # 'latents': latents, 'mus': mus, 'sigmas': sigmas, 'Zs': Zs
#   #return {'zgivenx_flat': zgivenx_flat,'MIWAE_LB_epoch': MIWAE_LB_epoch,'time_train': time_train,'time_impute': time_impute,'time_miwae': time_miwae,'imp_weights': imp_weights,'MSE': mse_train, 'xhat': xhat, 'mask': mask}
# 
# 
